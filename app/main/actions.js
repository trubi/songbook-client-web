// @flow

import { Song } from "../types"


export const SONG__SELECTED = "song/selected"
export const songSelected = (songId: ?string) => ({
    type: SONG__SELECTED,
    payload: {
        songId: songId,
    },
})
export const SONG__UPDATED = "song/updated"
export const songUpdated = (song: Song) => ({
    type: SONG__UPDATED,
    payload: {
        song: song,
    },
})

export const PAGE_LOADED = "PAGE_LOADED"
export const pageLoaded = () => ({
    type: PAGE_LOADED,
})

export const SONGS_LOADED = "SONGS_LOADED"
export const songsLoaded = (songs: Song[]) => ({
    type: SONGS_LOADED,
    payload: {
        songs: songs,
    },
})

export const SEARCH_CHANGED = "SEARCH_CHANGED"
export const searchChanged = (text: ?string) => ({
    type: SEARCH_CHANGED,
    payload: {
        text: text,
    },
})

export const SAVE_SONG_CLICKED = "SAVE_SONG_CLICKED"
export const saveSongClicked = (id: string, content: string, title: string, author: string) => ({
    type: SAVE_SONG_CLICKED,
    payload: {
        id: id,
        content: content,
        author: author,
        title: title,
    },
})

export const SCRAPE_SONG_CLICKED = "SCRAPE_SONG_CLICKED"
export const scrapeSongClicked = (songId: string) => ({
    type: SCRAPE_SONG_CLICKED,
    payload: {
        songId: songId,
    },
})

export const TRANS_CLICKED = "TRANS_CLICKED"
export const transClicked = (songId: string, amount: number) => ({
    type: TRANS_CLICKED,
    payload: {
        songId: songId,
        amount: amount,
    },
})
