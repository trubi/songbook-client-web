// @flow

import { serialize, parse } from "cookie"


export const setCookie = (name: string, value: string, exp: number = 1) => {
    document.cookie = serialize(name, value, {
        maxAge: exp * 24 * 60 * 60,
        path: "/",
    })
}

export const deleteCookie = (name: string) => {
    document.cookie = serialize(name, "", {
        maxAge: -1,
        path: "/",
    })
}

export const getCookie = (name: string, serverReq: ?Object): ?string => {
    const cookie = parse(serverReq
        ? (serverReq.headers && serverReq.headers.cookie) || ""
        : document.cookie)

    if (!cookie || !cookie[name]) {
        return null
    }

    return cookie[name]
}
