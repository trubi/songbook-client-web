// @flow

import React from "react"
import Document, { Head, Main, NextScript } from "next/document"


export default class MyDocument extends Document {

    render() {
        return (
            <html lang="en">
                <Head>
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/antd/3.8.1/antd.min.css" />
                    <style type="text/css">
                        {`
                        .song-content-wrapper p { font-size: 22px; line-height: 2; }
                        .song-content-wrapper sup { font-size: 26px; color: red; }
                        
                        .song-content-wrapper-print p { font-size: 14px; line-height: 2; }
                        .song-content-wrapper-print sup { font-size: 18px; color: red; font-weight: bold; }
                        `}
                    </style>
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        )
    }
}
