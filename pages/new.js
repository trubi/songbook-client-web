// @flow

import * as React from "react"
import data from "../app/data"

class Main2Page extends React.Component<Props> {
    props: Props

    render() {
        return (
            <div className="container">
                <div style={{
                    padding: 12,
                }}>
                    {data.sort((a, b) => a.author.localeCompare(b.author)).map(song => (
                        <div>
                            {song.title && (
                                <h1 style={{ fontSize: 28 }}>
                                    {song.author} – {song.title}
                                </h1>
                            )}
                            {song.content && (
                                <div
                                    className="song-content-wrapper-print"
                                    dangerouslySetInnerHTML={{ __html: `<p>${song.content}</p>` }} />
                            )}
                            <div style={{ "page-break-before": "always" }} />
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default Main2Page
