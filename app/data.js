
export default [
  {
    "content": "<sup>E</sup> Chlapeček <sup>C</sup> brečí <sup>E</sup> nesmíš ho <sup>C#</sup> hladit<br>\n" +
        "<sup>E</sup> Když ho budeš <sup>A</sup> hladit <sup>F#</sup> tak ho můžeš <sup>H</sup> zabít<br>\n" +
        "<sup>E</sup> Seš víla <sup>C</sup> lipís <sup>E</sup> máš bílý <sup>C#</sup> auto<br>\n" +
        "<sup>E</sup> Všechny nás <sup>A</sup> vozíš a <sup>F#</sup> to máme <sup>H</sup> za to<br>\n" +
        "<sup>E</sup> Že známe <sup>C</sup> heslo <sup>E</sup>, <sup>C#</sup><br>\n" +
        "<sup>E</sup> Že známe <sup>A</sup> heslo <sup>F#</sup>, <sup>H</sup><br>\n" +
        "<sup>E</sup>, <sup>F</sup>Lipís mi kája <sup>E</sup>, <sup>F</sup><br>\n" +
        "<br>\n" +
        "R:<sup>E</sup>, <sup>G</sup>, <sup>E</sup>",
    "youtube": "ZDyHYBA154Y",
    "supermusic": "5753",
    "id": "fda7f7ab-43c0-4a4e-a59a-716ac3ae409d",
    "author": "Vypsaná Fixa",
    "title": "1982"
  },
  {
    "content": " <sup>H</sup> Co tě to <sup>C#</sup> hned po rámu na <sup>E</sup> padá<br> <sup>H</sup> nohy ruce <sup>C#</sup> komu je chceš <sup>E</sup> dát.<br> <sup>H</sup> Je to v krvi <sup>C#</sup> co tvou hlavu <sup>E</sup> přepadá,<br> <sup>H</sup> chtělas padnout <sup>C#</sup> do hrobu a <sup>E</sup> spát.<br><br>Ref: <sup>D</sup> Poraněni <sup>A</sup> andělé jdou <sup>G</sup> do polí,<br> <sup>D</sup> stěhovaví <sup>A</sup> lidi ulí <sup>G</sup> taj<br> <sup>D</sup> panenku <sup>A</sup> bodni, ji to ne <sup>G</sup> bolí,<br> <sup>D</sup> svět je <sup>A</sup> mami, prapodivnej <sup>G</sup> kraj.<br><br>Po ránu princezna je ospalá,<br>na nebi nemusí se bát<br>v ulicích doba zlá ji spoutala,<br>polykač nálezů a ztrát<br><br>Ref:<br><br>Co tě to as po ránu napadá,<br>za zrcadlem nezkoušej si lhát<br>miluju tě, chci tě - to ti přísahám,<br>na kolenou lásce pomož vstát.<br><br>Ref:",
    "youtube": "72-m3wmkSjo",
    "supermusic": "185273",
    "id": "7c1bf4df-6683-4628-895f-218adb53e4b3",
    "author": "Wanastowi Vjecy",
    "title": "Andělé"
  },
  {
    "content": " <sup>G#mi</sup> Vymyslel jsem spoustu nápadu , a <sup>H</sup> hú<br>co <sup>G#mi</sup> podporujou hloupou nála <sup>F#</sup> du, a <sup>D#</sup> hú,<br> <sup>G#mi</sup> hodit klíče do kanálu,<br> <sup>C#</sup> sjet po zadku <sup>C#mi</sup> holou skálu<br> <sup>G#mi</sup> v noci chodit <sup>D#</sup> strašit do hra <sup>G#mi</sup> du<br>Dám si dvoje housle pod bradu, ahú,<br>v bíle plachtě chodím pozadu, ahú,<br>úplně melancholicky,<br>s citem pro věc jako vždycky,<br>vyrábím tu hradní záhadu, ahú.<br><br>Ref: <sup>H</sup> Má drahá dej mi víc, <sup>D#</sup> má drahá dej mi víc,<br> <sup>G#mi</sup> má drahá <sup>E</sup> dej mi víc své <sup>H</sup> lásky, a <sup>F#</sup> hú<br> <sup>H</sup> Ja nechci skoro nic, <sup>D#</sup> já nechci skoro nic,<br> <sup>G#mi</sup> já chci jen <sup>E</sup> pohladit tvé <sup>H</sup> vlásky, a <sup>D#</sup> hú<br><br>Nejlepší z těch divnej nápadu, a-hú,<br>mi dokonale zvednul náladu, a-hú,<br>natrhám ti sedmikrásky,<br>tebe celou s tvými vlásky,<br>zamknu si na sedm západů, a-hú<br><br>Ref:",
    "youtube": "0W-oBNYi_Eo",
    "supermusic": "1136562",
    "id": "6ae454fb-bfe0-490e-bfa9-7b5e1736c0ab",
    "author": "Olympic",
    "title": "Dej mi víc své lásky"
  },
  {
    "content": "sloky: <sup>F#mi</sup> <sup>A</sup> <sup>E</sup> <sup>F#mi</sup> <sup>A</sup> <sup>H</sup> <br>refren: <sup>E</sup> <sup>F#mi</sup> <sup>A</sup> <sup>H</sup> <br><br>V posledním vagónu oslepen světlem,<br>semletý životem po celý čas, vstávám a padám<br>A taky se dívám kam až jsem došel a pořád nevím kde je dole<br>Nic neodmítám, nic nežádám, nic netuším<br><br>Ref: I'm goin' blind and it's so fine<br>I'm goin' down and it's O.K.<br><br>A dávno už nevnímám nic, jen tvoje teplo, jediný světlo<br>v posledním vagónu, teď už to chápu<br>Po celý čas vstávám a padám<br>Nic neodmítám, nic nežádám, nic netuším<br><br>Ref: ",
    "youtube": "MW3YmTy09zI",
    "supermusic": "2982",
    "id": "b16b0157-e253-4a9e-aa51-11b56b6b998c",
    "author": "Mňága A Žďorp",
    "title": "Goin' blind"
  },
  {
    "content": " <sup>G</sup> Povídej jestli té má <sup>D</sup> hodně rád víc než <sup>G</sup> já<br> <sup>G</sup> Jestli když večer jdeš <sup>D</sup> spát ti polibek <sup>G</sup> dá<br>tak jako <sup>Hmi</sup> já ,to už je <sup>C</sup> dávno, tak poví <sup>G</sup> dej, <sup>D</sup> jen poví <sup>G</sup> dej<br>Povídej nechal té být, vždyť měl té tak rád<br>Nebos ho nechala jít, když šel k jiný spát<br>tak jako mne, to už je dávno,tak povídej,jen povídej<br><br>Ref: <sup>Hmi</sup> Povídej, <sup>G</sup> jestli se ti po mě <sup>A</sup> stýská<br> <sup>C</sup> když jdeš večer <sup>G</sup> spát<br> <sup>G</sup> Jestli když večer se b <sup>A</sup> lízká<br> <sup>C</sup> nepřestala ses <sup>G</sup> bát<br><br>Povídej ne já se nevrátím, bež domů spát<br>Svou lásku ti vyplatím, víc nemůžu dat",
    "youtube": "1cR6Gb7sWIo",
    "supermusic": "1049133",
    "id": "04d76b26-5112-4e67-bd06-e3ea2dbb14fc",
    "author": "Petr Novák",
    "title": "Povídej"
  },
  {
    "content": "Paš o paňori<br>Paš o paňori<br><sup>Ami7</sup>čhajori roma<sup>E7</sup>ňi<br>me la igen <sup>Ami7</sup>kamav  <br>-2x<br><sup>A7</sup>(<sup>Edim</sup>)<br>Joj mamo, <sup>Dmi7</sup>joj ma<sup>G7</sup>mo<br>de man <sup>C</sup>paňi, ma<sup>F</sup>mo<br>de man pa<sup>Dmi7</sup>ňi, ma<sup>E7</sup>mo<br>de man pa<sup>Ami7</sup>ňi<br><sup>A7</sup> (<sup>Edim</sup>)<br>Joj mamo, <sup>Dmi7</sup>joj ma<sup>G7</sup>mo<br>de man <sup>C</sup>paňi, ma<sup>F</sup>mo<br>de man pa<sup>Dmi7</sup>ňi, ma<sup>E7</sup>mo<br>de man pa<sup>Ami7</sup>ňi<br>Paš o paňori<br>bešelas korkori<br>pre ma užarelas<br>Joj mamo, joj mamo<br>de man paňi, mamo<br>de man paňi, mamo<br>de man paňi",
    "youtube": null,
    "supermusic": "18641",
    "id": "c4693d41-8e1d-4cb9-92b8-bb6778dbd978",
    "title": "Paš o paňori",
    "author": "Věra Bílá"
  },
  {
    "content": " <sup>C</sup> Půjdu si zašukat a <sup>G</sup> hned potom vož <sup>C</sup> rat.<br> <sup>C</sup> Vůbec se s tím nebudu <sup>G</sup> ani trochu <sup>C</sup> srat.<br>Mám domlu <sup>Dmi</sup> venou jednu kurvu v příze <sup>G</sup> mí,<br>že mi ho <sup>C</sup> drasticky vyhulí zdá se <sup>Ami</sup> mi.<br>Mám domlu <sup>Dmi</sup> venou jednu štětku z Kolí <sup>G</sup> na,<br>hned do hu <sup>C</sup> by jí pěkně cvaknu šul <sup>Ami</sup> ina.<br><br>Ref: Houpy <sup>G</sup> hou, houpy <sup>C</sup> hou<br>Než fune <sup>F</sup> bráci na kr <sup>G</sup> chov mě zdeku <sup>C</sup> jou<br><br>Včera jsem sbalil babu nechtěla mi dát.<br>Přece se kvůli tomu nebudu s ní prát.<br>Tak jsem jí lil na hlavu vodu z nádobí,<br>mívám občas takové divné období.<br>Pak jsem jí řekl z mého pokoje se kliď<br>a s její drahou šprndou vytřel jsem si řiť.<br><br>Ref:<br><br>V bordelu už mně všichni dávno tykají.<br>A k*rvy u silnice na mě mávají.<br>A moje libido je čím dál silnější,<br>ó dobrý Bože dej nám chleba vezdejší.<br>Já sám už nevím co s penisem počít mám,<br>asi ho ufiknu a do lihu ho dám<br><br>Ref:<br><br>S ženskejma bejvají jej samý potíže.<br>Málem jsem kvůli nim už mastil za mříže.<br>Naštěstí to však vždycky dobře dopadlo,<br>není totiž mrd*dlo jako mrd*dlo.<br>Jeden neví co v sobě ta která skrývá,<br>proto je lepší sedět v klidu u piva.",
    "youtube": "zYZ5skWqp_s",
    "supermusic": "156219",
    "id": "5089485f-096d-4df7-b958-fe4bd9ac8a0b",
    "author": "Záviš",
    "title": "Houpy Hou"
  },
  {
    "content": " <sup>F#mi</sup> Slejzaj mi nehty chřadnu řídne mi vlas<br> <sup>E</sup> to bych snad lidi nepřál nikomu z vás<br> <sup>F#mi</sup> celej se klepu čekám kdy to přijde<br> <sup>E</sup> mně je vám tak hrozně blbě takhle to dál<br> <sup>F#mi</sup> nejde<br><br>Nebudu slaboch zajdu za doktorem<br>ten by měl nejlíp vědět jak na tom jsem<br>trochu mě proklepal a pak zvedl zrak<br>řekl mi smutně chlape to máte tak<br><br> <sup>D</sup> Pevně se držte já řeknu vám<br> <sup>C#mi</sup> je to ta nejhorší nemoc kterou znám<br><br>Ref: <sup>H</sup> Žízeň <sup>A</sup> a ja ja ja <sup>E</sup> jaj (4x)<br><br>Nevím proč tenkrát ten číšník řval<br>když jsem mu recept s korunou dal<br>za patnáct piv který jsem na lístku měl<br>to se mu nezdálo co by ještě chtěl<br><br>Prostě jsem nemocnej tak proč máte zlost<br>myslíte že piju jen tak pro radost<br>Ref:<br><br>Prostě jsem nemocnej tak proč máte zlost<br>myslíte že piju jen tak pro radost<br>Ref:",
    "youtube": "-01-Nr1Bgiw",
    "supermusic": "2170",
    "id": "fb5a6bb4-26b2-4430-8554-3b996a29f453",
    "author": "Kabát",
    "title": "Žízeň"
  },
  {
    "content": " <sup>C</sup> Z Těšína <sup>Emi</sup> vyjíždí <sup>Dmi</sup> vlaky co <sup>G</sup> čtvrthodi <sup>C</sup> nu <sup>Emi</sup> <sup>Dmi</sup> <sup>G</sup> <br> <sup>C</sup> včera jsem <sup>Emi</sup> nespal a <sup>Dmi</sup> ani dnes <sup>G</sup> nespoči <sup>C</sup> nu <sup>Emi</sup> <sup>Dmi</sup> <sup>G</sup> <br> <sup>F</sup> svatý <sup>G</sup> Medard, můj <sup>C</sup> patron, ťuká si nače <sup>G</sup> lo<br>ale <sup>F</sup> dokud se <sup>G</sup> zpívá, <sup>F</sup> ještě se <sup>G</sup> neumře <sup>C</sup> lo <sup>Emi</sup> <sup>Dmi</sup> <sup>G</sup> <br><br>Ve stánku koupím si housku a slané tyčky,<br>srdce mám pro lásku a hlavu pro písničky,<br>ze školy dobře vím, co by se dělat mělo,<br>ale dokud se zpívá, ještě se neumřelo, hóhó.<br><br>Do alba jízdenek lepím si další jednu,<br>vyjel jsem před chvílí, konec je v nedohlednu,<br>za oknem míhá se život jak leporelo,<br>ale dokud se zpívá, ještě se neumřelo, hóhó.<br><br>Stokrát jsem prohloupil a stokrát platil draze,<br>houpe to, houpe to na housenkové dráze,<br>i kdyby supi se slítali na mé tělo,<br>tak dokud se zpívá, ještě se neumřelo, hóhó.<br><br>Z Těšína vyjíždí vlaky až na kraj světa,<br>zvedl jsem telefon a ptám se:\"Lidi, jste tam?\"<br>A z veliké dálky do uší mi zaznělo,<br>/: že dokud se zpívá, ještě se neumřelo. :/",
    "youtube": "6sDj8T2cnes",
    "supermusic": "703810",
    "id": "8ce4f37a-211e-46de-a265-1cd38f70a9bf",
    "author": "Jaromír Nohavica",
    "title": "Dokud se zpívá"
  },
  {
    "content": "<sup>Ami</sup>Nepi Jano, <sup>E7</sup>nepi <sup>Ami</sup>vo<sup>G</sup>du, <sup>C</sup>voda ti je <sup>G</sup>len na <sup>C</sup>škodu,<br><sup>C</sup>radšej ty <sup>G</sup>napi <sup>C</sup>ví<sup>Ami</sup>na, <sup>E7</sup>to je dobrá medi<sup>Ami</sup>cí<sup>G</sup>na,<br><sup>C</sup>radšej ty <sup>G</sup>napi <sup>C</sup>ví<sup>Ami</sup>na, <sup>E7</sup>to je dobrá medi<sup>Ami</sup>cína.<br><br>Načo sa ty za mnou vláčiš, keď ma ani neopáčiš,<br>ani večer, ani ráno, aký si ty sprostý Jano,<br>ani večer, ani ráno, aký si ty sprostý Jano.<br><br>Keby nebolo pršalo, bolo by ti dievča dalo,<br>ale že začalo pršať, nechcelo ti dievča držať,<br>ale že začalo pršať, nechcelo ti dievča držať.<br><br>Načo sú nám malé ženy, čo majú pri samej zemi,<br>a keď prší a je blato, všetko im to fŕka na to,<br>a keď prší a je blato, všetko im to fŕka na to.<br><br>Načo sú nám veľké ženy, na ktoré treba lešení,<br>a keď si v najlepšej chuti, lešenie sa s tebou zrúti,<br>a keď si v najlepšej chuti, lešenie sa s tebou zrúti. ",
    "youtube": "MoTOMnMk5dM",
    "supermusic": "865206",
    "id": "011ba14c-5aef-4d04-a00e-3b7194023141",
    "author": "Lidovky",
    "title": "Nepi Jano, nepi vodu"
  },
  {
    "content": "Táhněte <sup>C</sup> do háje - všichni <sup>Ami</sup> pryč<br>Chtěl jsem jít <sup>C</sup> do ráje a nemám <sup>Ami</sup> klíč <br>Jak si tu <sup>C</sup> můžete takhle <sup>Ami</sup> žrát?<br>Ztratil jsem <sup>F</sup> holku, co jí mám <sup>G</sup> rád <br> <br>Napravo, <sup>C</sup> nalevo nebudu mít <sup>Ami</sup> klid <br>Dala mi <sup>C</sup> najevo, že mě nechce <sup>Ami</sup> mít <br>Zbitej a <sup>C</sup> špinavej, tancuju <sup>Ami</sup> sám <br>Váš pohled <sup>Dmi</sup> káravej už dávno <sup>G</sup> znám <br> <br>Ref: Pořád jen <sup>F</sup> Na kolena, na kolena, na kolena, na kolena <sup>C</sup> Jé, jé, jé<br>Pořád jen: <sup>F</sup> Na kolena, na kolena, na kolena, na kolena <sup>C</sup> Jé, jé, jé<br>Pořád jen: <sup>F</sup> Na kolena, na kolena, na kolena, na kolena<br> <sup>C</sup> Je to <sup>Ami</sup> tak a vaše <sup>F</sup> saka vám posere <sup>G</sup> pták<br> <br>Cigáro do koutku si klidně dám <br>Tuhletu pochoutku vychutnám sám <br>Kašlu vám na bonton, Bejby si chytrej chlap<br>Sere mě Tichej Don a ten váš tupej dav<br> <br>Ref:<br>Ref:",
    "youtube": "wFj7rH6I2QM",
    "supermusic": "558",
    "id": "f87896b9-b379-4ba8-b581-5cc747b3753a",
    "author": "Ivan Hlas",
    "title": "Na kolena"
  },
  {
    "content": " <sup>Hmi</sup> Na dálnici <sup>G</sup> prázdný mžourám z oken<br> <sup>Hmi</sup> Odbila půlnoc a <sup>G</sup> dochází mi palivo<br> <sup>Hmi</sup> Nepospíchám <sup>A</sup> auto sunu krokem<br> <sup>E</sup> Šetřim zbytky par<br><br> <sup>Hmi</sup> A tu v místech kde <sup>G</sup> včera byly lesy<br> <sup>Hmi</sup> Pumpa stojí i s <sup>G</sup> velkou myčkou nalevo<br> <sup>Hmi</sup> Je to zvláštní že <sup>A</sup> člověk nevšimne si<br> <sup>E</sup> Taky maj tu bar<br><br>Ref: <sup>G</sup> Láďa Padrůněk se <sup>D</sup> krčí u lahváčů <sup>A</sup> Hůl ho podpírá<br> <sup>G</sup> Z nabídky jídel <sup>D</sup> Petr Novák v právě <sup>A</sup> Dlouze vybírá<br> <sup>G</sup> U kasy Mejla se <sup>D</sup> cvičně snaží sbalit <sup>A</sup> Novou pokladní<br> <sup>Hmi</sup> Zuzana N. jak na <sup>A</sup> jmenovce stojí <sup>E</sup> Je tu jen pár dní<br><br>A jak sem v šoku nenápadně platil<br>Miky Volek mi umyl zbylá vokna<br>Za mými zády se mihnul Milan Chladil<br>Už chci prosím ven<br><br>Ale vidím že Karel Zich mi nese<br>Cedulku s jménem a v hlavě se mi rovná<br>Že jsem usnul v tom nekonečným lese<br>Takže dobrý den<br><br>Ref:<br>",
    "youtube": "X9zyXqZaKC4",
    "supermusic": "89033",
    "id": "0890b2fb-20de-41fb-bd0a-2c84d09c7904",
    "author": "Tři Sestry",
    "title": "Pumpa"
  },
  {
    "content": "<sup> C</sup>Tlusté koberce plné <sup>Emi</sup> prachu<br><sup> A</sup>poprvé s holkou, <sup>F</sup> trochu <sup>G</sup> strachu<br><sup> C</sup>a stará dáma od vedle zas <sup>Emi</sup> vyvádí<br><sup> A</sup>zbyde tu po ní zvadlé <sup>F</sup> kapradí<br>a pár <sup>G</sup> prázdných flašek<br>veterán z legií nadává na revma<br>vzpomíná na Emu, jak byla nádherná<br>a všechny květináče už tu historku znají,<br>ale znova listy nakloní a poslouchají<br>vzduch voní kouřem.<br><br>Ref: <sup>C</sup> a svět je<br> <sup>Emi</sup> svět je jenom hodinový <sup>A</sup> hotel<br>a můj <sup>F</sup> pokoj je <sup>G</sup> studený a <sup>C</sup> prázdný<br><br>Ref:<br><br>vezmu si sako a půjdu do baru<br>absolventi kurzu nudy, pořád postaru<br>kytky v klopě vadnou, dívám se okolo po stínech:<br>Kterou? No přeci žádnou!<br>vracím se pomalu nahoru<br>cestou potkávám ty, co už padají dolů<br>a vedle v pokoji někdo šeptá:\"Jak ti je?\"<br>za oknem prší a déšť stejně nic nesmyje...<br><br>Ref:",
    "youtube": "3AlJtpTIm3o",
    "supermusic": "51",
    "id": "ab49a7fc-a585-435c-aa80-a995f098d0e6",
    "author": "Mňága A Žďorp",
    "title": "Hodinový hotel"
  },
  {
    "content": " <sup>C</sup> tramvají <sup>G</sup> dvojkou <sup>F</sup> jezdíval jsem <sup>G</sup> do Žide <sup>C</sup> nic <sup>G</sup> <sup>F</sup> <sup>G</sup> <br>z <sup>C</sup> tak velký <sup>G</sup> lásky <sup>F</sup> většinou <sup>G</sup> nezbyde <sup>Ami</sup> nic<br>z <sup>F</sup> takový <sup>C</sup> lásky <sup>F</sup> jsou kruhy <sup>C</sup> pod oči <sup>G</sup> ma<br>a dvě <sup>C</sup> spálený <sup>G</sup> srdce - <sup>F</sup> Nagasaki Hi <sup>G</sup> rošima <sup>C</sup> <sup>G</sup> <sup>F</sup> <sup>G</sup> <br><br>jsou jistý věci co bych tesal do kamene<br>tam kde je láska tam je všechno dovolené<br>a tam kde není tam mě to nezajímá<br>jó dvě spálený srdce - Nagasaki Hirošima<br><br>já nejsem svatej ani ty nejsi svatá<br>ale jablka z ráje bejvala jedovatá<br>jenže hezky jsi hřála, když mi někdy bylo zima<br>jó dvě spálený srdce - Nagasaki Hirošima<br><br>tramvají dvojkou jezdíval jsem do Židenic<br>z tak velký lásky většinou nezbyde nic<br>z takový lásky jsou kruhy pod očima<br>a dvě spálený srdce - Nagasaki Hirošima<br>a dvě spálený srdce - Nagasaki Hirošima<br>a dvě spálený srdce - Nagasaki Hirošima<br>a dvě spálený srdce - Nagasaki Hirošima",
    "youtube": "tdtKIgz8G60",
    "supermusic": "2393",
    "id": "4b485c9e-e6e2-494b-b55b-24a7c996a28d",
    "author": "Mňága A Žďorp",
    "title": "Nagasaki Hirošima"
  },
  {
    "content": "RánoKeď máš <sup>Ami</sup>doma opäť ďalší <sup>F</sup>problém<br>Keď fotra s <sup>G</sup>matkou berú <sup>C</sup>zas tie staré <sup>G</sup>mindrá<sup>Ami</sup>ky<br>Že neuspel si opäť v škole <br>Jak sa to správaš a čo to nosíš za známky<br>Keď frajerka ti spáva s iným<br>S ktorým si včera pil minimálne do rána<br>A sám si opäť zostal s tými ktorí ťa vedú<br>Nevieš <sup>G</sup>Kam ..<br>®: Keď včera bola <sup>Ami</sup>tma tak <sup>F</sup>Dnes príde <sup>G</sup>ráno<br>A s ním sa skončí <sup>C</sup>sen čo <sup>F</sup>kradol ti <sup>G</sup>spánok<br>A svitanie ti <sup>Ami</sup>úsmev na <sup>F</sup>tvári pripra<sup>G</sup>ví<br>A ty vyhráš ten boj o kľúče od klietky<br>A aj keď budeš sám Sám proti všetkým<br>Ešte nič nekončí Naopak všetko práve začína<br>Keď ti v televízii veľkí klamú<br>Všemožne snažia sa zakryť svoje omyly<br>A na ulici by ťa fízli za cigu z trávy<br>Najradšej do basy hodili<br>A kruh sa ti už uzatvára Už je ti jasné<br>Život nebeží jak by si chcel<br>Ešte ti tu zostáva Nádej A končí s tým<br>Prečo ..<br>®:",
    "youtube": "9ePtwClM71w",
    "supermusic": "2676",
    "id": "6273cf95-195f-47fa-a7a6-c5bc4feb141f",
    "title": "Ráno",
    "author": "Inekafe"
  },
  {
    "content": " <sup>C</sup> When I find myself in <sup>G</sup> times of trouble<br> <sup>Ami</sup> Mother Mary <sup>F</sup> comes to me<br> <sup>C</sup> Speaking words of <sup>G</sup> wisdom, let it <sup>F</sup> be <sup>C</sup> <br>And in my hour of <sup>G</sup> darkness<br>She is <sup>Ami</sup> standing right in <sup>F</sup> front of me<br> <sup>C</sup> Speaking words of <sup>G</sup> wisdom, let it <sup>F</sup> be <sup>C</sup> <br>Let it <sup>Ami</sup> be, let it <sup>G</sup> be<br>Let it <sup>F</sup> be, let it <sup>C</sup> be<br>Whisper words of <sup>G</sup> wisdom<br>Let it <sup>F</sup> be <sup>C</sup> <br>And when the broken hearted peopleYou'll understand<br>Living in the world agree<br>There will be no answer, let it be<br>But though there may be parted<br>There is still a chance that they will see<br>There will be an answer, let it be<br>And when the night is cloudy<br>There is still a light that shines on me<br>Shine until tomorrow, let it be<br>I wake up to the sound of musict it be<br>Mother Mary comes to me<br>Whisper words of wisdom, let it be ",
    "youtube": "ZtNU616XqQk",
    "supermusic": "354160",
    "id": "b38204d4-8121-4bbd-b0b8-0a311cb8fe95",
    "author": "Beatles",
    "title": "Let It Be"
  },
  {
    "content": "Marie <sup>Hmi</sup> má se vracet, ta, co tu bydlí<br>Marie má se vracet, tak postav <sup>Emi</sup> židli<br>Marie bílý racek, Marie <sup>Hmi</sup> má se vracet<br>ty si tu <sup>C#7</sup> dáváš dvacet, tak abys <sup>F#7</sup> vstal<br><br>Marie má se vracet, co by ses divil<br>Marie má se vracet, ta cos ji mydlil<br>Marie modrý ptáček, Marie moudivláček<br>S kufříkem od natáček, ta cos ji štval<br><br>Marie <sup>Hmi</sup> hoď sem cihlu, má se vracet, trá ra ta ta<br><br>Marie má se vracet, píšou že lehce<br>Marie má se vracet, že už tě nechce<br>Ta co jí není dvacet, Marie má se vracet<br>Ta cos jí dal pár facek a pak s ní spal<br>Marie, hoď sem...<br><br>Marie má se vracet, ta co tu bydlí<br>Marie má se vracet, tak postav židli<br>olej a těžký kola, vlak někam do Opola<br>herbatka, jedna Cola, no tak se sbal<br>Marie, hoď sem...<br><br>Marie <sup>Emi</sup> holubice<br>Marie <sup>Hmi</sup> létavice<br>Marie <sup>C#7</sup> blýskavice<br>krasavice, <sup>F#7</sup> ech - Rosice, Pardubice<br><br>Tak postav <sup>Emi</sup> židli<br>a <sup>F#7</sup> sbal si fidli <sup>Hmi</sup> ",
    "youtube": "MFNf6bMNXQU",
    "supermusic": "63271",
    "id": "05f8afbf-af72-40e6-804a-8b3bfca404b0",
    "author": "Zuzana Navarová",
    "title": "Marie"
  },
  {
    "content": "Až mě <sup>Emi</sup>zítra ráno v pět, <br><sup>G/D</sup>ke zdi postaví,<br><sup>C</sup>ještě si napos<sup>D</sup>led <br>dám <sup>G</sup>vodku na zdraví<sup>E</sup>,<br>z očí <sup>Ami</sup>pásku <sup>D4sus</sup>strhnu <sup>D</sup>si, <br>to abych <sup>G</sup>viděl <sup>Hmi/F#</sup>na ne<sup>Emi</sup>be<br>a <sup>Ami</sup>pak vzpomenu <sup>H7</sup>si, <br><sup>Emi</sup>lásko, na te<sup>Emi7</sup>be,<br><sup>Ami, D4sus, D,  G Hmi/F# Emi</sup><br>a <sup>Ami</sup>pak vzpomenu <sup>H7</sup>si na te<sup>Emi</sup>be.<br><br>Až zítra ráno v pět přijde ke mně kněz,<br>řeknu mu, že se splet', že mně se nechce do nebes,<br>že žil jsem, jak jsem žil, a stejně tak i dožiju<br>a co jsem si nadrobil, to si i vypiju,<br>a co jsem si nadrobil, si i vypiju.<br><br>Až zítra ráno v pět poručík řekne:\"Pal!\",<br>škoda bude těch let, kdy jsem tě nelíbal,<br>ještě slunci zamávám, a potom líto přijde mi,<br>že tě, lásko, nechávám, samotnou tady na zemi,<br>že tě, lásko, nechávám, na zemi.<br><br>Až zítra ráno v pět prádlo půjdeš prát<br>a seno obracet, já u zdi budu stát,<br>tak přilož na oheň a smutek v sobě skryj,<br>prosím, nezapomeň, nezapomeň a žij,<br>Lásko, nezapomeň a žij...",
    "youtube": "9L9f8-jG4DA",
    "supermusic": "114763",
    "id": "8014b47c-11d1-4c8a-b36f-1879aff53079",
    "author": "Jaromír Nohavica",
    "title": "Zítra ráno v pět"
  },
  {
    "content": " <sup>G</sup> Vzpomínám když tehdá <sup>C</sup> před lety <sup>G</sup> začaly lítat <sup>C</sup> rakety <br> <sup>G</sup> zdál se to bejt <sup>Emi</sup> docela do <sup>C</sup> brej ná <sup>D</sup> pad<br>Saxofony hrály unyle a frčely švédský košile<br>někdo se moh docela dobře flákat.<br><br>Když tam stál rohatej u školy my neměli podepsaný úkoly<br>už tenkrát rozhazoval svoje sítě<br>Poučen z předchozích nezdarů sestrojil elektrickou kytaru<br>a rock´n´roll byl zrovna narozený dítě<br><br>Ref: <sup>G</sup> Vzpomínáš, taky s tu <sup>D</sup> žila a <sup>Emi</sup> nedělej, že jsi ji <sup>C</sup> ná<br>taková <sup>G</sup> malá pilná <sup>D</sup> včela, <sup>C</sup> taková celá <sup>D</sup> Same <sup>G</sup> to <sup>C</sup> vá <sup>G</sup> <sup>C</sup> <sup>G</sup> <br><br>Přišel čas a jako náhoda byla tu bigbeatová pohoda<br>kytičky a úsmevy sekretárok.<br>Sousedovic bejby Milena je celá blbá z Boba Dylana<br>ale to nevadí, já mám taky nárok<br><br>Starý, mladý nebo pitomý mlátily do toho jako my<br>hlavu plnou Londýna nad Temží<br>Starej dobrej satanáš hraje u nás v hospodě mariáš<br>pazoura se mu trumfama jenom hemží<br><br>Ref: Vzpomínáš, už je to jinak a jde z toho na mě zima<br>ty jsi holka tehdá byla, taková celá Sametová<br><br>A do toho tenhle Gorbačov co ho znal celej Dlabačov<br>kopyta měl jako z Arizóny<br>Přišel a zase vodešel a nikdo se kvůli tomu nevěšel<br>a po něm tu zbyly samý volný zóny<br><br>Ref: Vzpomínáš, jak jsi se měla<br>když jsi nic nevěděla<br>byla to taková krásná cela a byla celá Sametová<br>",
    "youtube": "EDT3xfMc7_g",
    "supermusic": "807",
    "id": "cf8de271-d053-4d60-8a7c-397a846f8907",
    "author": "Žlutý Pes",
    "title": "Sametová"
  },
  {
    "content": " <sup>Ami</sup> Visím metr <sup>G</sup> nad zemí a <sup>Ami</sup> nejsem David <sup>G</sup> Copperfield,<br> <sup>Ami</sup> už nezkouším se <sup>G</sup> vůbec na nic <sup>C</sup> ptát <sup>E</sup> <br>Ty párkrát přejdeš ulici, nepřekonáš vůli, cit,<br>pak zlobně stojíš, na vlasech ti začíná sníh tát.<br> <sup>C</sup> Nechám tě tady, nechám tě tady,<br>neumřeš hlady, život tě baví.<sup>Ami,G</sup><br><br>Ref.: <sup>C</sup> Lepší časy <sup>Ami</sup> asi nepří <sup>G</sup> dou, Romea s Julií pohřbili pod křídou,<br>Pepík to udělal Marii před bídou, karty na Betlém a táhnou za kometou.<br><br>Pak už jsi nejistá, co se mi mohlo stát,<br>schody bereš po dvou a na půdě kroky tvý<br>rozvíří peří, co zbylo z holubů,<br>já peří nevířím nohy mám výš.<br>Nechám tě tady, nechám tě tady,<br>neumřeš hlady, život tě baví.<br><br>Ref:",
    "youtube": "VEo2WZqrVmY",
    "supermusic": "6885",
    "id": "6f30759f-438b-4c62-9bcf-b352143831f9",
    "author": "Tři Sestry",
    "title": "Lepší časy"
  },
  {
    "content": " <sup>Ami</sup> Nebe je modrý a <sup>E7</sup> zlatý, bílá sluneční <sup>Ami</sup> záře,<br> <sup>Ami</sup> horko a sváteční <sup>E7</sup> šaty, vřava a zpocený <sup>Ami</sup> tváře,<br> <sup>Ami</sup> dobře vím, co se bude <sup>E7</sup> dít, býk už se v ohradě <sup>Ami</sup> vzpíná,<br> <sup>Ami</sup> kdo chce, ten může <sup>E7</sup> jít, já si dám sklenici <sup>Ami</sup> vína.<br><br>Ref: <sup>Dmi</sup> Žízeň je veliká, <sup>Ami</sup> život mi utíká,<br> <sup>E7</sup> nechte mě příjemně <sup>Ami</sup> snít,<br> <sup>Dmi</sup> ve stínu pod fíky <sup>Ami</sup> poslouchat slavíky,<br> <sup>E7</sup> zpívat si s nima a <sup>Ami</sup> pít.<br><br>Ženy jsou krásný a cudný, mnohá se ve mně zhlídla,<br>oči jako dvě studny, vlasy jak havraní křídla,<br>dobře vím, co zname ná pád do nástrah dívčího klína,<br>někdo má pletky rád, já si dám sklenici vína.<br><br>Ref:<br><br>Nebe je modrý a zlatý, ženy krásný a cudný,<br>mantily, sváteční šaty, oči jako dvě studny,<br>zmoudřel jsem stranou od lidí, jsem jak ta zahrada stinná,<br>kdo chce, ať mi závidí, já si dám sklenici vína.<br><br>Ref: ",
    "youtube": "Ifimsaport8",
    "supermusic": "866343",
    "id": "24a72593-f5f1-45a5-8604-7ee8ee50b859",
    "author": "Waldemar Matuška",
    "title": "Slavíci z Madridu"
  },
  {
    "content": " <sup>G</sup> No tak mi <sup>Emi</sup> zatancuj, ať <sup>D</sup> náladu mám,<br> <sup>G</sup> A dej mi <sup>Emi</sup> celou noc, já <sup>D</sup> nechci bejt <sup>G</sup> sám.<br>Hlavní je neuh <sup>Emi</sup> nout, dobře <sup>D</sup> zvolit svůj <sup>G</sup> směr,<br>a teď holka musim <sup>Emi</sup> jít až <sup>D</sup> tam na se <sup>G</sup> ver.<br><br>Ref: Cestu <sup>G</sup> znám a <sup>C</sup> neměním <sup>G</sup> směr, <sup>Emi</sup> <sup>D</sup> <sup>G</sup> <br>dojdu k řece plný ryb, až tam, na sever.<br><br>Procházím krajinou a lidi mě zvou,<br>čím těžší víno, lehčí holky tu sou,<br>jedna z nich povídá - dokud dávám tak ber,<br>já jí jenom políbil a šel na sever.<br><br>Ref:<br><br>Mám nohy bolavý už nechtěj se hnout,<br>tou temnou vodou nechám tělo svý plout,<br>zakončím s noblesou ze všech poslední den,<br>kam mě vlny donesou, tam vsákne mě zem.<br><br>Ref:<br><br>Tá <sup>D</sup> cesta byla rovná, místy rozbitá,<br> <sup>C</sup> číše vína plná, jindy celá vylitá,<br>už <sup>D</sup> nevrátím se zpátky, ubejvá mi sil ..<br>Tak <sup>C</sup> řekněte jí, prosím, že <sup>D</sup> sem tady byl.<br><br>Ref:",
    "youtube": "iFqPo-REjWg",
    "supermusic": "3473",
    "id": "02313bb7-6432-4b05-ad17-18bf4774b6df",
    "author": "Kabát",
    "title": "Na sever"
  },
  {
    "content": " <sup>Esmaj7</sup> musíme zajet na chatu<br> <sup>Dmi7</sup> musím se podívat na tatu<br> <sup>Cmi7</sup> chtěl bych se za ním podívat<br> <sup>Gmi</sup> tralalalálálá ...<br><br><sup>Esmaj7, Dmi7, Cmi7, Gmi, Esmaj7, Dmi7, Cmi7, Gmi<br>Bmaj7</sup>Tata, tata, <sup>Dmi7</sup> tata tata tata tata <sup>Cmi7<br>Bmaj7</sup>Tata, tata, <sup>Dmi7</sup> tata tata tata tata<sup>Cmi7<br></sup><br>Dneska ho stáhnem na pivo<br>Jenom se rýpe do hlíny<br>má tam ty svoje okurky<br>tralalalálálá ...<br><br>Nasaď ty staré tepláky<br>všecko co umím je od taty<br>musíme zajet na chatu<br>za mojim tatu<br><br>musíme zajet na chatu<br>musím se podívat na tatu<br>chtěl bych se za ním podívat<br>tralalalálálálalala ...<br><br>Nasaď ty staré tepláky<br>všecko co umím je od taty<br>musíme zajet na chatu<br>za mojim tatu<br><br>jo ..<br>®: //: Tata, tata, tata, tata. ://<br>",
    "youtube": "T3jMOT65cow",
    "supermusic": "922",
    "id": "4875bd7d-5688-4bb3-ac13-20dcc03f4390",
    "author": "Buty",
    "title": "Tata"
  },
  {
    "content": "/: <sup>Ami</sup> Ide poštár i <sup>E7</sup> de telegram jej ne <sup>Ami</sup> sie.:/<br>/: <sup>Ami</sup> Ona ho <sup>Dmi</sup> čítala, čitá <sup>G</sup> la, vlasy si <sup>C</sup> trhála, trhá <sup>Ami</sup> la,<br>choď poštár, <sup>Dmi</sup> choď poštár, choď poš <sup>E7</sup> tár, neľúbim <sup>Ami</sup> ťa./: <sup>A7</sup> <br>/:Ide poštár ide, telegram mi nesie.:/<br>/:Ja som ho čítala, čítala, vlasy si trhala, trhala<br>joj počkaj, joj počkaj - joj počkaj, lásku mi vráť.:/<br>/:O poštáris ável telegamos avel.:/<br>/:Sarmele, gináva, gináva, opálo čingéra, čingéra<br>džamóre, džamóre, džamóre na kavag dú.:/",
    "youtube": "L_64rjr7R40",
    "supermusic": "865216",
    "id": "9ac2b175-52df-40ba-93a5-746e53cb358a",
    "author": "Lidovky",
    "title": "Ide poštár ide"
  },
  {
    "content": " <sup>G</sup> Modrá je <sup>D</sup> planeta, kde <sup>C</sup> můžeme <sup>D</sup> žít<br> <sup>G</sup> Modrá je <sup>D</sup> voda, kterou <sup>C</sup> musíme <sup>D</sup> pít<br> <sup>G</sup> Modrá je <sup>D</sup> obloha, když <sup>C</sup> vodejde <sup>D</sup> mrak<br>Modrá je <sup>F</sup> dobrá, <sup>C</sup> už je to tak <sup>G</sup> .<br><br>Modrá je Milka - ta naše kráva<br>Modrá je prej v Americe tráva<br>Modrá je údajně i polární liška<br>Senzačně modrá je moje vojenská knížka.<br><br>Ref: Jako <sup>C</sup> nálada když zahrajou poslední <sup>G</sup> kus<br>Modrá je <sup>F</sup> naděje <sup>C</sup> láska i <sup>F</sup> moje <sup>G</sup> blues<br>Je to <sup>C</sup> barva, kterou mám prostě <sup>G</sup> rád<br>Modrá je dob <sup>F</sup> rá, už <sup>C</sup> je to <sup>G</sup> tak.<br><br>Modrá je Raketa - ta moje holka<br>modrá je vzpomínka na Mikyho Volka<br>velká rána je modrej přeliv<br>modrý voko má i černej šerif<br><br>Ref: ",
    "youtube": "8NjJzaBQBqo",
    "supermusic": "989",
    "id": "98ba6276-aa03-4901-ac24-d027165d31cf",
    "author": "Žlutý Pes",
    "title": "Modrá"
  },
  {
    "content": "Ak nie si moja <sup>Em</sup> Ak nie si <sup>D/H</sup>moja, tak potom neviem <sup>C/A</sup>čia si<br> <sup>Em</sup> Máš detskú <sup>D/H</sup>tvár a <sup>C/A</sup>hrozne dobré vlasy<br> <sup>Em</sup> Za tisíc <sup>D/F#</sup>dotykov <sup>G</sup> postačí <sup>Cmaj7</sup> jediný <sup>D</sup> <br> <sup>Em</sup> Skúsme byť <sup>D/F#</sup>obaja <sup>G</sup> vinní aj <sup>Cmaj7</sup> bez viny O <sup>Hm7</sup> ó<br>Ak nie si moja, potom neviem čia si<br>Máš detskú tvár a hrozne dobré vlasy<br>Cestičkou vo vlasoch prídem ti v ústrety<br>Pesnička o nás dvoch už nie je pre deti <sup>Hm7</sup> <br><br>Ref: Všetci nám <sup>Em</sup> môžu závi <sup>Cmaj7</sup> dieť<br>Vravia, že <sup>D</sup> lásky dávno <sup>D#dim7</sup> niet<br>Všetci nám <sup>Em</sup> môžu závi <sup>Cmaj7</sup> dieť<br>Že Ťa mám <sup>D</sup> rá <sup>D#dim7</sup> d<br>Zostaňme <sup>Em</sup> takto chvíľu <sup>Cmaj7</sup> stáť<br>Na všetko <sup>D</sup> krásne máme <sup>D#dim7</sup> čas<br>Viem, že si <sup>Em</sup> práve práve <sup>Cmaj7</sup> tá ktorú mám <sup>D</sup> rád <sup>H</sup> <br><br> <sup>Em</sup> Ak nie si <sup>D/H</sup>moja, tak potom neviem <sup>C/A</sup>čia si<br> <sup>Em</sup> Máš detskú <sup>D/H</sup>tvár a <sup>C/A</sup>hrozne dobré vlasy<br> <sup>Em</sup> Za tisíc <sup>D/F#</sup>dotykov <sup>G</sup> postačí <sup>Cmaj7</sup> jediný <sup>D</sup> <br> <sup>Em</sup> Skúsme byť <sup>D/F#</sup>obaja <sup>G</sup> vinní aj <sup>Cmaj7</sup> bez viny O <sup>Hm7</sup> ó<br><br>Ref: ",
    "youtube": "Yfa1s7xdAWc",
    "supermusic": "840222",
    "id": "da215640-3129-4844-a919-635e6ca02c65",
    "author": "Vašo Patejdl",
    "title": "Ak nie si moja"
  },
  {
    "content": "Cesta je <sup>Ami</sup> prach a <sup>G</sup> šterk a <sup>Dmi</sup> udusaná<br> <sup>Ami</sup> hlína <sup>C</sup> a šedé <sup>F</sup> šmrouhy <sup>G7</sup> kreslí do vla <sup>C</sup> sů<br>a z hvězdných <sup>Dmi</sup> drah má <sup>G</sup> šperk co <sup>c</sup> kamením<br>se <sup>Emi</sup> spína <sup>Ami</sup> a pírka <sup>G</sup> touhy<br>z <sup>Emi</sup> křídel pega <sup>Ami</sup> sů.<br><br>Cesta je bič, je zlá jak pouliční dáma, má<br>v ruce štítky a pase staniol, a z očí chvíč jí plá,<br>když háže do neznáma, dvě křehké snítky<br>rudých gladiol.<br><br>Ref: <sup>G</sup> Seržante písek je bílý jak paže Daniely<br> <sup>Ami</sup> počkejte chvíli mé oči uviděli<br> <sup>G</sup> tu strašne dávnou vteřinu zapomnění<br> <sup>Ami</sup> Seržante mávnou <sup>G7</sup> a budem zasvěceni<br> <sup>C</sup> Morituri te salutant <sup>E</sup> , morituri te salutant ...<br><br>Tou cestou dál jsem šel, kde na zemi se zmítá<br>a písek víří křídla holubí a marš mi hrál<br>zvuk děl co uklidnění skrýtá,<br>a zvedá chmýři které zahubí.<br><br>Cesta je tér a prach a udusaná hlína<br>mosazná včelka od vlkodlaka<br>Rezavý kvér, můj prach a sto let stará špína<br>a děsně velká bíla oblaka.",
    "youtube": "BWcLkj74mv4",
    "supermusic": "1677",
    "id": "4e503cf6-aac5-4d27-9713-ba19544d7a36",
    "author": "Karel Kryl",
    "title": "Morituri te salutant"
  },
  {
    "content": " <sup>Cmi</sup> Cez cestu mi prebehli dve <sup>Fmi</sup> líšky<br> <sup>Gmi</sup> muž a <sup>Cmi</sup> žena <sup>Fmi</sup> <sup>Gmi</sup> <br> <sup>Cmi</sup> Na ten film som nekupoval <sup>Fmi</sup> lístky<br> <sup>Gmi</sup> zmena bola <sup>Fmi</sup> vyhradená<br>Do nočnej tmy im pálili štyri oči<br>boli to slovania<br>v hlave sa im točí ako na kolotoči<br>od milovania<br>Na večeru si dali dva malé holuby<br>a potom miesto spania<br>či sa vám to ľúbi a či neľúbi<br>pustili sa do milovania<br><br>Ref: Milo <sup>Cmi</sup> vanie, <sup>Fmi</sup> milo <sup>Gmi</sup> vanie až do <sup>Cmi</sup> výšok <sup>Fmi</sup> <sup>Gmi</sup> <br>áno <sup>Cmi</sup> i nie, <sup>Fmi</sup> milo <sup>Gmi</sup> vanie dvoch <sup>Fmi</sup> líšok<br><br>Potom si vzali hotel do prenájmu<br>žiadna skepsa<br>Pokiaľ niečo lepšie si nenájmu<br>stačí búdka pre psa<br>Strecha nad hlavou vždycky musí byť<br>líška sa zlostí<br>Zišiel by sa im asi vlastný byt<br>neradi to robia na verejnosti<br><br>Ref:<br><br> <sup>Cmi</sup> Cez cestu mi prebehli dve <sup>Fmi</sup> líšky <sup>Gmi</sup> <sup>Cmi</sup> <sup>Fmi</sup> <sup>Gmi</sup> <br> <sup>Cmi</sup> ktoré na mňa kašlali <sup>Fmi</sup> z výšky... <sup>Gmi</sup> <sup>Cmi</sup> ",
    "youtube": "-OVZ_LbzrUI",
    "supermusic": "5053",
    "id": "9ec23bc1-12c1-42da-be5d-1bf7f12a8877",
    "author": "Richard Müller",
    "title": "Dve líšky"
  },
  {
    "content": " <sup>C</sup> Mám pěknou sirku v zubech, <sup>Gmi</sup> krempu do čela<br>A <sup>F</sup> bota zpuchřelá mi <sup>As</sup> vrásky nedělá<br> <sup>C</sup> jen tou svou sirkou škrtni <sup>G</sup> ať se ohřejem<br>Až <sup>C</sup> přikryje nás zem <sup>F</sup> tak už si neškrtnem<br><br>Ref: <sup>C</sup> Má, lásko, <sup>Gmi</sup> jen ty smíš kázat mi <sup>F</sup> nad hrobem <sup>As</sup> <br> <sup>C</sup> Má zem pak <sup>G</sup> bude lehká, bude mi <sup>C</sup> lehká zem<br>Bude mi <sup>F</sup> lehká ze <sup>C</sup> m, aá <sup>Gmi</sup> á .. <sup>Ami, F, Fmi</sup><br><br>Mám tu svá vysvědčení, pohlednici z Brém<br>A proti pihám krém, co s nimi čert je vem<br>Ten čert sám brzo zjistí že má v podpaží<br>moc těžké závaží a někde v poli rozpaží<br><br>Ref:<br><br>Tak dobré ráno milé myši v kostelích<br>tak ať vam chutná klíh všech křídel andělských<br>A dobrý večer sovo, která myši jíš<br>ptáš se zdali mi vzali zavčas spali<br>jářku, to si píš<br><br>Ref:<br><br>Už nemám ani klobouk, pluje povětřím<br>Jak zvony hledá Řím, či sebe sama, co já vím<br>Jen to že co mám tebe už tíží necítím<br>Áááá ..<br><br>Ref:",
    "youtube": "meJVC4v1d3I",
    "supermusic": "4016",
    "id": "e1ee31a1-aa96-4835-97ea-ee02758750bb",
    "author": "Jana Kirschner",
    "title": "Bude mi lehká zem"
  },
  {
    "content": " <sup>Emi</sup> <br> <sup>G</sup> Nandej mi <sup>D</sup> do hlavy tvý <sup>Ami</sup> brouky<br>A Bůh nám seber bezna <sup>G</sup> děj<br>V duši zbylo <sup>D</sup> světlo z jedný <sup>Ami</sup> holky<br>Tak mi teď za to vyna <sup>G</sup> dej<br>Zima a <sup>D</sup> promarněný <sup>Ami</sup> touhy<br>Do vrásek stromů padá <sup>G</sup> déšť<br>Zbejvaj roky <sup>D</sup> asi ne moc <sup>Ami</sup> dlouhý<br>Do vlasů mi zabrou <sup>C</sup> kej pá <sup>D</sup> pa pá <sup>G</sup> pá<br><br>Ref: pá <sup>D/F#</sup>pá pa <sup>Emi</sup> pá, pá pa pa <sup>G</sup> pá<br>pá <sup>D/F#</sup>pá pa <sup>Emi</sup> pá, pá pa pa <sup>G</sup> pá<br><br>Tvoje oči jenom žhavý tóny<br>Dotek slunce zapadá<br>Horkej vítr rozezní mý zvony<br>Do vlasú ti zabrouká pá pa pa pá<br><br>Ref:<br><br>Na obloze křídla tažnejch ptáků<br>Tak už na svý bráchy zavolej<br>Na tváře ti padaj slzy z mraků<br>A Bůh nám sebral beznaděj<br>V duši zbylo světlo z jedný holky<br>Do vrásek stromů padá déšť<br>Poslední dny hodiny a roky<br>Do vlasů ti zabrouká pá pa pa pá,<br><br>Ref:",
    "youtube": "9SW-q1XPkMQ",
    "supermusic": "455390",
    "id": "9d3dee46-4a8b-4a31-9b0b-8a934e4ad0cd",
    "author": "Lucie",
    "title": "Amerika"
  },
  {
    "content": " <sup>C</sup> Zase je pátek a <sup>G</sup> mám toho dost.<br>Tak beru <sup>B</sup> kramle, za zády <sup>F</sup> černý most.<br>A <sup>C</sup> pokud mi to D1 <sup>G</sup> dovolí,<br>než minu <sup>B</sup> Jihlavu, <sup>F</sup> hodím Prahu za hla <sup>C</sup> vu.<br><br>Jedu jak pražák, hla <sup>G</sup> va nehlava,<br>mám <sup>B</sup> jediný cíl a <sup>F</sup> přede mnou je Pála <sup>C</sup> va.<br>Už od Brna jsem jako <sup>G</sup> na trní,<br>ta <sup>B</sup> sladká píseň tak <sup>F</sup> blízko zní.<br><br>Ref: <sup>C</sup> My máme rádi <sup>G</sup> víno,<br> <sup>B</sup> milujeme <sup>F</sup> krásné ženy a <sup>C</sup> zpěv<br>a na věc jdeme <sup>G</sup> přímo<br> <sup>B</sup> v žilách nám proudí <sup>F</sup> moravská <sup>C</sup> krev (2x)<br><br> <sup>C</sup> Jako papír <sup>G</sup> hodím práci <sup>B</sup> a všechny šéfy <sup>F</sup> za hlavu<br> <sup>C</sup> Pánové <sup>G</sup> omlouvám se, ale <sup>B</sup> já mizím na <sup>F</sup> Moravu.<br><br> <sup>C</sup> Najdu tam všechno, <sup>G</sup> co mi doma schází,<br> <sup>B</sup> dobrý lid a <sup>F</sup> klid a řád.<br> <sup>C</sup> Najdu tam všechno, co <sup>G</sup> na světě mám rád<br> <sup>B</sup> vodu, slunce a <sup>F</sup> nekonečný vinohrad.<br><br>Ref:<br><br>Tak se tu žije na <sup>F</sup> padesátý rovnoběž <sup>G</sup> ce<br>V mírném klima našeho podnebí<br> <sup>Em</sup> Je krásné nemít <sup>F</sup> nikdy dost<br> <sup>G</sup> a dělat věci jen tak pro radost!<br><br>Ref: ",
    "youtube": "gOqv-VBrXeA",
    "supermusic": "782930",
    "id": "d0231f30-74ec-4fa8-af43-cdbd48cad6e6",
    "author": "Chinaski",
    "title": "Vinaři"
  },
  {
    "content": "<sup>Ami</sup>Ovečky <sup>Fmaj7</sup>zaběhnu<sup>G</sup>té, <sup>C</sup>kdo vás včil <sup>Fmaj7</sup>pozhá<sup>G</sup>ňá,<br>  <sup>Dmi</sup>bača mňa <sup>G</sup>opustil, <sup>Ami</sup>už je  za <sup>Emi</sup>hora<sup>Ami</sup>ma.<br>  <br>  Ovečky moje zlaté, slunéčko zapadá,<br>  co mi to udělal, měla sem ho ráda.<br>  <br>  Ovečky poztrácané, ohrada je prázdná,<br>  bača mňa opustil, ostala sem sama ...<br>  <br>  Ovečky zaběhnuté ...<br>  <br>  Ovečky moje zlaté ...<br>  <br>  Ovečky poztrácané ...<br>      ",
    "youtube": "YR354NgFu5E",
    "supermusic": "1203",
    "id": "1528766c-1fad-4259-a925-473f1561e892",
    "author": "Fleret",
    "title": "Ovečky zaběhnuté"
  },
  {
    "content": "Otázky<sup>C</sup> <sup>F</sup> <sup>G</sup> <sup>C</sup> <sup>C</sup> <sup>F</sup> <sup>G</sup> <sup>C</sup> <br>1. <sup>C</sup>Kolik mám ještě <sup>C7</sup>dní ,<sup>F</sup>než přprijde poslední<br>  <sup>G</sup>jak dlouho budu <sup>F</sup>zpí<sup>G</sup>vat a <sup>C</sup>hrát<br>  kolik je na zemi cest, kterou mám dát se vést<br>  nebo už myslet mám na návrat<br>  ®: <sup>C</sup>Posečkej lásko <sup>C7</sup>má okamžik<sup>F</sup>,<br>  <sup>D7</sup>vždyť svět je veliký otazník<sup>G</sup><br>  pořád se jenom ptáš a odpovedi tý se nedockáš<br>  ©: <sup>E</sup>Ja jen vím v zime strom <sup>Ami</sup>nekvete,<br>  <sup>F</sup>v lete sníh <sup>E</sup>nepadá, v noci je <sup>Ami</sup>tma<br>  <sup>Ami</sup>rád tě mám,<br>  jen - <sup>F</sup>neptej se <sup>C</sup>proč,nevím <sup>G</sup>sám<br>  <br> <sup>C</sup> <sup>F</sup> <sup>G</sup> <sup>C</sup> <sup>C</sup> <sup>F</sup> <sup>G</sup> <sup>C</sup><br>2. Kde je tvůj dětský smích a proč je láska hřích<br>  kolik jen váží bolest člověčí<br>  proč je zlato drahý kov<br>  a proč je tolik prázdnych slov<br>  proč chce kazdý být největší.<br> ®:<br>  © Já jen vím řeky proud hučí dál,<br>  v žilách krev penivá,kdo jí tam dal<br>  rád tě mám, jen neptej se proč, nevím sám<br>  3. Kolik je slunci let ,milión nebo pět,<br>  myslíš, že zítra ráno vyjde zas<br>  kolik je ulic ,měst, proč umí kytky kvést,<br>  proč nikdo neslyší můj hlas    <br>  ",
    "youtube": "QBhLnCOYzKU",
    "supermusic": "467",
    "id": "a30af35c-2a7f-48be-a9b0-af9341172930",
    "title": "Otázky",
    "author": "Olympic"
  },
  {
    "content": " <sup>C</sup> Já studoval a studoval a budoucnost si budoval<br>Má máma chtěla, aby ze mě zeměměřič <sup>G</sup> byl<br>Však <sup>F</sup> v běhu věcí nastal zlom, já došel jsem si pro diplom<br>Teď už mě hřeje v kapse, čímž se <sup>G</sup> splnil mámin <sup>C</sup> cíl <sup>G</sup> <br><br>Mé nadání je všestranné, jsem bystrý, moje sestra ne<br>Však strojařina potkala mě asi omylem<br>Proč já jen na tu školu lez, když nevymyslím vynález<br>A nikdy nepochopím to perpetum mobile<br><br>Ref: <sup>C</sup> Promovaní inženýři, sínus, kosínus, deskriptíva<br> <sup>G</sup> Promovaní inženýři, kdo je tu inženýr, ať s námi zpívá<br> <sup>C</sup> Promovaní inženýři, sínus, kosínus, deskriptíva<br> <sup>G</sup> Promovaní inženýři, hlavy študova <sup>C</sup> né<br><br>Nemám IQ vynálezce ani za jménem CSc.<br>Však mám rád hudbu, čož je myslím povahový klad<br>Pracujem ani nemrknem, když rýsujeme za prknem<br>Tak není divu, že si chceme taky zazpívat<br><br>I když jsem v jádru technokrat, na banjo umím zabrnkat<br>Můj kolega zas na kontrabas slušně basuje<br>Tak přesto, že jsme vzdělaní teď plivneme si do dlaní<br>A budem všichni hrát a zpívat, kolik nás tu je<br><br>Ref:",
    "youtube": "fK-SowwNUwc",
    "supermusic": "1156",
    "id": "e8412848-b8e3-46e8-beb1-c9fe6638bd90",
    "author": "Plavci",
    "title": "Inženýrská"
  },
  {
    "content": "Hledali mě včera večer <sup>Dmi</sup> fízli<br>Já však <sup>B</sup> zrovna nebyl doma, já byl <sup>C</sup> na červeném mostě<br> <sup>F</sup> o zábradlí <sup>B</sup> opřen <sup>F</sup> prostě. <sup>C</sup> <br><br> <sup>F</sup> Hledali mě <sup>A</sup> včera večer <sup>Dmi</sup> fízli <sup>A</sup> <br> <sup>F</sup> hledali mě <sup>A</sup> včera večer <sup>Dmi</sup> fízli<br>Já však <sup>B</sup> zrovna nebyl doma, já byl na <sup>C</sup> červeném mostě<br> <sup>F</sup> a na něm jsem <sup>B</sup> kouřil <sup>F</sup> prostě. <sup>C</sup> <br><br>Ref: <sup>F</sup> Červený most <sup>A</sup> je <sup>Dmi</sup> železniční most <sup>B</sup> ,<br> <sup>F</sup> víc vám k tomu <sup>C</sup> nesdělím už <sup>B</sup> to je dost, to jedost, to je <sup>F</sup> dost.<br> <sup>F</sup> Červený most <sup>A</sup> je <sup>Dmi</sup> železniční most <sup>B</sup> ,<br> <sup>F</sup> víc vám k tomu <sup>C</sup> nesdělím už <sup>F</sup> to je dost. <sup>C</sup> <br><br>Nenašli mě včera večer fízli<br>Nenašli mě včera večer fízli<br>poněvadž já nebyl doma, já byl na červeném mostě<br>a z toho mostu plival prostě<br><br>Nenašli mě včera večer fízli<br>Nenašli mě včera večer fízli<br>nemohli já nebyl doma, já byl na červeném mostě<br>a z něj dolů čuměl prostě.<br><br>Ref:<br><br>Až mě příště budou hledat fízli.<br>Až mě příště budou hledat fízli,<br>Budu na Červeném mostě, vzplanou ve mě libé touhy<br>vždyť jsem jenom člověk pouhý.<br><br>Až mě příště budou hledat fízli, johó!<br>Až mě příště budou hledat fízli,<br>Budu na Červeném mostě, spokojeně onanovat,<br>jak se zdravý muž má chovat.",
    "youtube": "n_YsojR09Lk",
    "supermusic": "150402",
    "id": "66cd7e93-7c6e-402e-9628-6690b82cb40b",
    "author": "Záviš",
    "title": "Červený most"
  },
  {
    "content": " <sup>A</sup> Poď sem, nech sa s tebou zblížim,<br> <sup>E</sup> poď sem, ja ti neublížim,<br> <sup>Hmi</sup> poď sem, ja ťa nezbijem,<br> <sup>D</sup> I'm sorry, I'm Lesbian.<br>Poď sem, som tvoj dvorný básnik,<br>mám energiu za dvanástich,<br>a k tomu lubrikačný gel,<br>I'm sorry, I'm really Gay.<br><br>R: <sup>A</sup> Lesbian's and Gay's <sup>E</sup> song...<br> <sup>G</sup> Lesbian's and Gay's <sup>D</sup> song... 2x<br><br>Poď sem, pustím Iron Maiden,<br>poď sem, nalejem ti za jeden,<br>poď sem, s bárským nepijem,<br>I'm sorry, I'm Lesbian.<br>Poď sem, tu si ku mne hačaj,<br>keď som ťa pozval na rum a čaj.<br>Stoj! Nechoď nikam! Čo je? Hej!<br>I'm sorry, I'm really Gay.<br><br>Ref.<br>Recit.<br><br>Poď sem, spolu máme v pláne,<br>že ťa okúpem vo fontáne,<br>tak otvor bránu, lebo ju rozbijem!<br>I'm sorry baby, I'm Lesbian.<br>Viem, neopakuj mi to stále,<br>ja som vyrástol na Death Metal-e.<br>Ja takýmto veciam rozumiem.<br>I'm sorry... ale veď ja viem - Lesbian<br>Ref.",
    "youtube": "0GCf2_nYvAE",
    "supermusic": "10140",
    "id": "02d80733-91fe-4a42-8d71-6b023f09cd1c",
    "author": "Horkýže Slíže",
    "title": "L. A. G. song"
  },
  {
    "content": "Na <sup>Ami</sup> celý svet sa dívam <sup>D</sup> cez okno<br>lebo <sup>F</sup> všade dobre doma najlep <sup>C</sup> šie <sup>E</sup> <br>Na celý svet zabúdam cez okno<br>po ruskej vodke ho mám najväčšie<br> <sup>Fmaj7</sup> Chicago Paríž, Dilí Róma<br> <sup>Ami</sup> Cez okno vidím a sedím doma <sup>F</sup> <sup>Ami</sup> <br> <sup>F</sup> Chicago Paríž, Dilí Róma<br> <sup>Ami</sup> Cez okno vidím a sedím doma <sup>F</sup> <sup>Ami</sup> <br><br>Ref: Cez ok <sup>F</sup> no si môžme tykať<br>cez ok <sup>D</sup> no sa môžme stýkať<br>cez ok <sup>C</sup> no vyletí dýka<br>cez ok <sup>E</sup> no zastrelím býka<br>cez ok <sup>F</sup> no sa krásne hýka<br>cez ok <sup>D</sup> no urazím strýka<br>cez ok <sup>C</sup> no budeme pykať<br>cez ok <sup>E</sup> no skúšame dýchať<br><br>Na celý svet sa dívam cez okno<br>doma je najlepšie a všade je dobre<br>Celý svet zabúdam cez okno<br>každý to okno odo mňa žobre<br>Čechov, Maďarov a svet dindov<br>pozorne sledujem cez svoj Window<br><br>Ref:",
    "youtube": "FTTVa62ZI-A",
    "supermusic": "2130",
    "id": "5e609e5a-8836-4bc0-a952-3af38a0c1d51",
    "author": "Jaro Filip",
    "title": "Cez okno"
  },
  {
    "content": " <sup>Ami</sup> Spatřil jsem kometu, oblohou letěla,<br>chtěl jsem jí zazpívat, ona mi zmizela,<br> <sup>Dmi</sup> zmizela jako laň <sup>G7</sup> u lesa v remízku,<br> <sup>C</sup> v očích mi zbylo jen <sup>E7</sup> pár žlutých penízků.<br>Penízky ukryl jsem do hlíny pod dubem,<br>až příště přiletí, my už tu nebudem,<br>my už tu nebudem, ach, pýcho marnivá,<br>spatřil jsem kometu, chtěl jsem jí zazpívat.<br><br>Ref: <sup>Ami</sup> O vodě, o trávě, <sup>Dmi</sup> o lese,<br> <sup>G7</sup> o smrti, se kterou smířit <sup>C</sup> nejde se,<br> <sup>Ami</sup> o lásce, o zradě, <sup>Dmi</sup> o světě<br> <sup>E</sup> a o všech lidech, co kdy <sup>E7</sup> žili na téhle <sup>Ami</sup> planetě.<br><br>Na hvězdném nádraží cinkají vagóny,<br>pan Kepler rozepsal nebeské zákony,<br>hledal, až nalezl v hvězdářských triedrech<br>tajemství, která teď neseme na bedrech.<br>Velká a odvěká tajemství přírody,<br>že jenom z člověka člověk se narodí,<br>že kořen s větvemi ve strom se spojuje<br>a krev našich nadějí vesmírem putuje.<br><br>Ref:<br><br>Spatřil jsem kometu, byla jak reliéf<br>zpod rukou umělce, který už nežije,<br>šplhal jsem do nebe, chtěl jsem ji osahat,<br>marnost mne vysvlékla celého donaha.<br>Jak socha Davida z bílého mramoru<br>stál jsem a hleděl jsem, hleděl jsem nahoru,<br>až příště přiletí, ach, pýcho marnivá,<br>my už tu nebudem, ale jiný jí zazpívá.<br><br>Ref:",
    "youtube": "dQrC0mSRNUk",
    "supermusic": "47613",
    "id": "f9f1f43e-c1a4-42c6-a616-1bc850b4d752",
    "author": "Jaromír Nohavica",
    "title": "Kometa"
  },
  {
    "content": " Do tvých <sup>Dmi</sup> očí jsem se zbláznil a teď <sup>E7</sup> nemám nemám <sup>Gmi</sup> klid,<br> <sup>Ami</sup> hlava třeští, asi tě mám <sup>Dmi</sup> rád.<br> <sup>Dmi</sup> Stále někdo říká, vzbuď se, <sup>E7</sup> věčně trhá <sup>Gmi</sup> nit,<br> <sup>Ami</sup> studenou sprchu měl bych si <sup>Dmi</sup> dát.<br><br>Ref: <sup>D7</sup> Na pouti jsem vystřelil <sup>Gmi</sup> růži z papíru,<br> <sup>C7</sup> dala sis ji do vlasů, kde <sup>F</sup> hladívám tě <sup>A7</sup> já<br> <sup>Dmi</sup> v tomhle smutným světě jsi má <sup>Gmi</sup> naděj na víru,<br>že <sup>Ami</sup> nebe modrý ještě smysl <sup>Dmi</sup> má.<br><br>Přines jsem ti kytku, no co koukáš, to se má,<br>tak jsem asi jinej, teď to víš.<br>Možná trochu zvláštní<br>v dnešní době, no tak ať,<br>třeba z ní mou lásku vytušíš.<br><br>Ref:",
    "youtube": "vS6stq9GVLU",
    "supermusic": "1235",
    "id": "a70c4ea8-b305-4807-b716-fd71286a2c2c",
    "author": "Brontosauři",
    "title": "Růže z papíru"
  },
  {
    "content": " <sup>A</sup> Jednou mi fotr <sup>A7</sup> povídá<br> <sup>D</sup> Zůstali jsme už sami dva<br> <sup>E</sup> A že si chce začít taky trochu <sup>A</sup> žít <br> <br> <sup>A</sup> Nech si to projít <sup>A7</sup> palicí<br> <sup>D</sup> A nevracej se s opicí<br> <sup>E</sup> Snaž se mě, hochu, trochu pocho <sup>A</sup> pit<br> <br>Ref: Já <sup>E</sup> šel, šel dál, Baby, <sup>A</sup> kam mě Pánbůh zval<br>Já <sup>E</sup> šel, šel dál, Baby, a <sup>D7</sup> furt jen tancoval<br> <sup>A</sup> Na každý divný hranici<br> <sup>D</sup> Na policejní stanici<br> <sup>E</sup> Hrál jsem jenom rock and roll for <sup>A</sup> you<br> <br>Přiletěl se mnou černej čáp<br>Zobákem dělal rocin'klap<br>A nad kolíbkou Elvis Presley stál <br> <br>Obrovskej bourák v ulici<br>Po boku krásnou slepici<br>A lidi šeptaj: Přijel ňákej král <br> <br>Ref:<br> <br>Pořád tak ňák nemohu<br>Chytit štěstí za nohu<br>A nemůžu si najít klidnej kout<br> <br>Bláznivý ptáci začnou řvát<br>A nový ráno šacovat<br>A do mě vždycky pustí silnej proud <br>",
    "youtube": "E4Ci4FkKs9I",
    "supermusic": "555",
    "id": "162c8577-ad80-4f62-b26c-e64ea84a35ba",
    "author": "Ivan Hlas",
    "title": "Jednou mi fotr povídá"
  },
  {
    "content": " <sup>D</sup> U stánků <sup>G</sup> na levnou krásu<br> <sup>D</sup> postávaj a <sup>Emi</sup> smějou se času<br>s <sup>D</sup> cigaretou a s <sup>A</sup> holkou co nemá kam <sup>D</sup> jít.<br><br> <sup>D</sup> Skleniček pár a <sup>G</sup> pár tahů z trávy<br> <sup>D</sup> uteče den jak <sup>Emi</sup> večerní zprávy<br> <sup>D</sup> neuměj žít a <sup>A</sup> bouřej se a neposlou <sup>D</sup> chaj.<br><br>Ref: Jen <sup>G</sup> zahlídli svět maj <sup>A</sup> na duši vrásky<br>tak <sup>D</sup> málo je, <sup>Emi</sup> málo je lásky<br> <sup>D</sup> ztracená víra <sup>A</sup> hrozny z vinic neposbí <sup>D</sup> rá.<br><br> <sup>D</sup> U stánků <sup>G</sup> na levnou krásu<br> <sup>D</sup> postávaj a <sup>Emi</sup> ze slov a hlasů<br> <sup>D</sup> poznávám jak <sup>A</sup> málo jsme jim stačili <sup>D</sup> dát.<br><br>Ref:",
    "youtube": "nyRJYxLGkhA",
    "supermusic": "756736",
    "id": "fadb8a8e-7f7c-4f31-bb82-4c42f052301c",
    "author": "Brontosauři",
    "title": "Stánky"
  },
  {
    "content": " <sup>A</sup> Modlitba stoupá <sup>C#mi</sup> Na oblacích pěny<br> <sup>F#mi</sup> Není tak hloupá <sup>H</sup> pro ty zasvěcený<br> <sup>E</sup> Velekněz láhví <sup>D</sup> dlaně rozevírá<br> <sup>H</sup> Andělé dáví <sup>D</sup> a shůry padá síra<br><br>Na nebe vstoupí tahle prosbička krátká<br>A že nejsou skoupí tak jí nezabouchnou vrátka<br>Splněná přání brzy se vrátí<br>A to čekání kolo fernetů zkrátí<br><br> <sup>A</sup> Voňavý holky, karty, <sup>C#mi</sup> placatý káry<br>Hory, <sup>F#mi</sup> motorku, řízek a <sup>H</sup> lakovaný máry<br> <sup>A</sup> Prastarý vína, stříbro, <sup>C#mi</sup> humry, slávky<br> <sup>F#mi</sup> Abelovi od kokaina <sup>H</sup> sociální dávky<br>Vránovi pádlo a rum pro Korkorána<br>V Třebíči na recepci překvapení zrána<br>Moře do Písku, sobě malý pole ropy<br>A aby tu Tři sestry dlouho zanechaly stopy<br><br>Štěkotu psímu odpovíme sborem<br>nečuchat k vínu a před půlnocí sbohem<br>Srpnový kůže a minimální šaty<br>riskujou úžeh a tak vytáhli paty<br><br>My už to víme nic neni podle plánu<br>tak tu sedíme nakloněni flámu<br>Ve stavu nouze koupeme se v řece<br>čekáme dlouze kdyby jednou přece...<br><br>Ref:",
    "youtube": "7xf-t_oCjMM",
    "supermusic": "63938",
    "id": "6639159f-3dd1-448b-88c7-c766ea067d58",
    "author": "Tři Sestry",
    "title": "Modlitba pro partu"
  },
  {
    "content": "Hey <sup>F</sup> Jude don't make it <sup>C</sup> bad<br>take a <sup>C7</sup> sad <sup>C7sus</sup> song and <sup>C7</sup> make it <sup>F</sup> better<br>re <sup>B</sup> member to let her into your <sup>F</sup> heart<br>then you can <sup>C7</sup> start to make it <sup>F</sup> better.<br><br>Hey Jude don't be afraid<br>you were made to go out and get her<br>the minute you let her under your skin<br>then you begin to make it better.<br><br>Ref: <sup>F7</sup> And any time you feel the <sup>B</sup> pain hey <sup>B/A</sup>Jude ref <sup>Gmi7</sup> rain,<br>don't <sup>Gmi7/F</sup>carry the <sup>C7/E</sup>world <sup>C7</sup> upon your <sup>F</sup> shoulders<br> <sup>F7</sup> for well you know that it's a <sup>B</sup> fool who <sup>B/A</sup>plays it <sup>Gmi7</sup> cool<br>by <sup>Gmi7/F</sup>making his <sup>C7/E</sup>world a <sup>C7</sup> little <sup>F</sup> colder<br>da da da <sup>F7</sup> da da da da <sup>C7</sup> da da.<br><br>Hey Jude don't let me down<br>you have found her now go and get her<br>remember to let her into your heart<br>then you can start to make it better.<br><br>Ref: So let it out and let it in hey Jude begin<br>you're waiting for someone to perform with<br>and don't you know that it's just you hey Jude you'll do<br>the movement you need is on your shoulders<br>da da da da da da da da da.<br><br>Hey Jude don't make it bad<br>take a sad song and make it better<br>remember to let her under your skin<br>then you'll begin to make it better<br>better better better better better oh.",
    "youtube": "A_MjCqQoLLA",
    "supermusic": "3179",
    "id": "89bd1fff-c44b-41b4-a298-c89c9ecdfed3",
    "author": "Beatles",
    "title": "Hey Jude"
  },
  {
    "content": " <sup>D</sup> Za lasami <sup>G</sup> za gorami <sup>A</sup> za dolina <sup>D</sup> mi<br>pobili sie <sup>G</sup> dwaj gorale <sup>A</sup> ciupagami<br><br>/: ej gora <sup>G</sup> le <sup>A</sup> nie bijta <sup>D</sup> sie<br>ma goralka <sup>G</sup> dwa warkocze<br> <sup>A</sup> podzielita <sup>D</sup> sie :/<br><br> <sup>D</sup> Za lasami <sup>G</sup> za gorami <sup>A</sup> za dolina <sup>D</sup> mi<br>pobili sie <sup>G</sup> dwaj gorale <sup>A</sup> ciupagami<br><br>/: ej gora <sup>G</sup> le <sup>A</sup> nie bijta <sup>D</sup> sie<br>ma goralka <sup>G</sup> dwoje oczu<br> <sup>A</sup> podzielita <sup>D</sup> sie :/<br><br> <sup>D</sup> Za lasami <sup>G</sup> za gorami <sup>A</sup> za dolina <sup>D</sup> mi<br>pobili sie <sup>G</sup> dwaj gorale <sup>A</sup> ciupagami<br><br>/: ej gora <sup>G</sup> le <sup>A</sup> nie bijta <sup>D</sup> sie<br>ma goralka <sup>G</sup> wielke serce<br> <sup>A</sup> podzielita <sup>D</sup> sie :/<br><br> <sup>D</sup> Za lasami <sup>G</sup> za gorami <sup>A</sup> za dolina <sup>D</sup> mi<br>pobili sie <sup>G</sup> dwaj gorale <sup>A</sup> ciupagami<br><br>/: ej gora <sup>G</sup> le <sup>A</sup> nie bijta <sup>D</sup> sie<br>ma goralka <sup>G</sup> z przodu z tylu<br> <sup>A</sup> podzielita <sup>D</sup> sie :/",
    "youtube": "OXtiMyAfRK8",
    "supermusic": "14603",
    "id": "9f987bd9-a539-4c6b-84f0-3750af4f46b5",
    "author": "Lidovky",
    "title": "Gorale"
  },
  {
    "content": " <sup>G</sup> Na hladinu rybníká svítí sluníčko<sup>C<br>Emi</sup>A kolem stojí v hustém kruhu <sup>G</sup> topoly<sup><br>Ami</sup>Které tam zasadil jeden hodný <sup>Hmi</sup> člověk<sup><br>Ami</sup>Jmenoval se František <sup>D</sup> Dobrota<br>František Dobrota, rodák z blízke vesnice<br>Měl hodně dětí a jednu starou babičku<br>Která když umírala tak mu řekla: \"Františku,<br>teď dobře poslouchej, co máš všechno udělat !\"<br><br>Ref: /: <sup>C</sup> Balabambam, balabambam <sup>C</sup> <sup>D</sup> <sup>C</sup> :/ 3x<br> <sup>Ami</sup> a kolem rybníka nahusto nasázet <sup>D</sup> topoly<br><br>František udělal všechno co mu řekla<br>A po snídani poslal děti do školy<br>Žebřiňák s nářadím dotáhl od chalupy k rybníku<br>Vykopal díry a zasadil topoly<br>Od té doby vítr na hladinu nefouká<br>Takže je klidná jako velké zrcadlo<br>Sluníčko tam svítí vždycky rádo<br>protože tam vidí Františkovu babičku<br>",
    "youtube": "V4KqOjveYnE",
    "supermusic": "4",
    "id": "686a3498-6a84-4edf-87bd-759bd16ce1c2",
    "author": "Buty",
    "title": "František"
  },
  {
    "content": "V <sup>C</sup> mládí jsem se učil hrobařem<br> <sup>Ami</sup> jezdit s hlínou, jezdit s trakařem<br> <sup>F</sup> kopat hroby byl můj ide <sup>G</sup> ál <sup>G7</sup> <br><br>Jezdit s hlínou, jezdit s vozíkem<br>s černou rakví, bílým pomníkem<br>toho bych se nikdy nenadál.<br><br>Že do módy příjde kremace<br>černej hrobař bude bez práce<br>toho bych se nikdy nenadál nenadál.<br><br>Kolem projel vůz milionáře<br>záblesk světel pad mi do tváře,<br>marně skřípěj kola brzdící.<br><br>Stoupám vzhůru stoupám ke hvězdám<br>tam se s černou rakví neshledám,<br>sbohem bílé město zářící.<br><br>Sbohem moje mesto<br>vzpominat budu presto<br>jak jsem poznal tvůj smich a tvůj pláč<br>Na na na .......",
    "youtube": "K9INZr1Mir8",
    "supermusic": "62094",
    "id": "4d862a3b-6cb4-459f-a084-866f8abbfb7f",
    "author": "Premier",
    "title": "Hrobař"
  },
  {
    "content": "<sup>Emi</sup>Eště som sa něoženil <sup>H7</sup>už ma žena <sup>Emi</sup>bije<br>a já som si narychtoval <sup>H7</sup>tri dubové <sup>Emi</sup>ky<sup>D</sup>je<br><br>/: <sup>G</sup>S jedným budzem ženu bici<br><sup>Ami</sup>a s tým druhým <sup>D</sup>dzeci, dzeci, <sup>H7</sup>dzeci, dzeci<br><sup>Emi</sup>a s tým tretím kyja, kyja<sup>Ami</sup>čiskom<br><sup>Emi</sup>pojdzem <sup>H7</sup>na zá<sup>Emi</sup>lety :/<br><br>Kam ty zajdeš aj já zajdu pojdzeme do mlýna<br>opýtáme sa mlynára čo že za novina<br><br>/: Kolečka sa otáčajů<br>žitečko sa mele, mele, mele, mele<br>moja milá sa vydává<br>pojdzem na veselie :/",
    "youtube": "qLMnwwQc0jM",
    "supermusic": "6400",
    "id": "137c8dfb-3193-4e77-8397-de6afeef366d",
    "author": "Lidovky",
    "title": "Eště som sa něoženil"
  },
  {
    "content": " <sup>Gmi</sup> Ako mladé diev <sup>Cmi7</sup> ča <sup>Eb</sup> už je to <sup>D</sup> za nami<sup>G4 Gmi<br>Gmi</sup>Po stromoch som <sup>Cmi7</sup> liezla s  <sup>Eb</sup> našimi <sup>D</sup> chlapca<sup>G4 Gmi</sup>mi.<br> <sup>B</sup> A vždy keď na strome <sup>F/A</sup>zlomili halúzku,<br><sup>D7/4</sup>hádzali <sup>D7</sup> mi chlapci <sup>Eb</sup> čerešne <sup>F</sup> za blúz<sup>G4 Gmi</sup>ku<br> <sup>B</sup> keď boli zvedaví, čo to mám <sup>F/A</sup>pod blúzkou<br><sup>D7/4</sup>odháňa <sup>D7</sup> la som ich <sup>Eb</sup> zelenou <sup>F</sup> halúz<sup>G4 Gmi</sup>kou.<br><br>Ubránila som sa viac menej úspešne,<br>nikto nesmel siahnuť na moje čerešne.<br>Neverte mládencom, keď sa Vám zapáčia<br>vyjedia vám všetky čerešne z koláča.<br>Chlapci neublížia kým hľadia zo stromu<br>keď zoskočia dolu, čakajte pohromu.<br><br>Keď sa na vás chlapec zadíva uprene,<br>ako tie čerešne budete červené.<br>Keď sa na vás šuhaj usmeje pod fúzky<br>rýchlo vyberajte čerešne zpod blúzky.<br>Čerešne sú zrelé a blúzka priúzka<br>nič vám nepomôže zelená halúzka.<br>",
    "youtube": "eOaBOgRRTEA",
    "supermusic": "556",
    "id": "1f80f781-ce0b-4a69-87e3-507c35ce9311",
    "author": "Hana Hegerová",
    "title": "Čerešne"
  },
  {
    "content": " <sup>Emi</sup> Bylas jak poslední hlt <sup>F#</sup> vína<br> <sup>H7</sup> sladká a k ránu bola <sup>Emi</sup> vá <sup>C</sup> <sup>H7</sup> <br> <sup>Emi</sup> za nehty výčitky a <sup>F#</sup> špína<br> <sup>H7</sup> říkalas dnešní noc je <sup>Emi</sup> tvá <sup>C</sup> <sup>H7</sup> <br><br>Ref: <sup>G</sup> Co my dva z lásky vlastně <sup>H7</sup> máme<br> <sup>Ami</sup> hlubokou šachtou padá <sup>H7</sup> zdviž, já říkám kiš kiš<br> <sup>Emi</sup> navrch má vždycky těžký <sup>F#</sup> kámen<br> <sup>H7</sup> a my jsme koncích čím dál <sup>Emi</sup> blíž <sup>C</sup> <sup>H7</sup> <sup>Emi</sup> <br><br>Ame tuha nádživava <sup>Ami</sup> jaj dari dari <sup>H7</sup> daj!<br> <sup>Emi</sup> Ame tuha nádživava <sup>Ami</sup> jaj dari dari <sup>H7</sup> daj!<br><br>Zejtra tě potkám za svým stínem<br>neznámí známí v tramvaji<br>v cukrárně kávu s Harlekýnem<br>hořká a sladká splývají<br><br>Ref:<br><br>Ame tuha nádživava jaj dari dari daj!<br><br>Bylas jak poslední hlt vína<br>zbývá jen účet zaplatit - a jít<br>za nehty výčitky a špína<br>a trapný průchod banalit<br><br>Ref:<br><br>Ame tuha nádživava jaj dari dari daj!<br><br>Navrch má vždycky těžký kámen<br>a my jsme v koncích čím dál blíž",
    "youtube": "MVQMchtT5Co",
    "supermusic": "358773",
    "id": "183caf50-b931-4cdd-82d0-f0b598a1d086",
    "author": "Zuzana Navarová",
    "title": "Já s tebou žít nebudu"
  },
  {
    "content": "Jako <sup>C</sup> suchej starej strom, jako <sup>Fmaj7</sup> všeničící hrom, jak v poli <sup>C</sup> tráva,<br>připa <sup>C</sup> dá mi ten náš svět, plnej <sup>Fmaj7</sup> řečí a čím <sup>Ami</sup> víc tím <sup>G</sup> líp se <sup>C</sup> mám.<br><br>Ref: Budem <sup>Fmaj7</sup> o něco se rvát, až tu <sup>Ami</sup> nezůstane <sup>G</sup> stát na kameni <sup>C</sup> kámen,<br>a jestli <sup>Fmaj7</sup> není žádnej Bůh, tak nás <sup>Ami</sup> vezme země <sup>G</sup> vzduch, no a potom <sup>C</sup> amen.<br><br>A to všechno proto jen že pár pánů chce mít den bohatší králů,<br>přes všechna slova co z nich jdou, hrabou pro kuličku svou jen pro tu svou.<br><br>Ref:<br><br>Možná jen se mi to zdá a po těžký noci přijde přijde hezký ráno,<br>jaký bude nevím sám, taky jsem si zvyk na všechno kolem nás.<br><br>Ref:",
    "youtube": "Yj2tEoFllAE",
    "supermusic": "1226",
    "id": "b211136b-22ce-4058-b605-ec89fd4a880a",
    "author": "Brontosauři",
    "title": "Na kameni kámen"
  },
  {
    "content": " <sup>D</sup> jako je sama skála<br> <sup>Gmaj7</sup> tak jsem sám i <sup>Emi</sup> já <sup>A</sup> <br>jako je prázdná duha<br>tak jsem prázdný i já<br>jako je zrádná voda<br>tak zradím i já<br><br>Ref: <sup>D</sup> zkouším se prokopat ven - zkouší se prokopat ven!<br> <sup>Gmaj7</sup> zkouším se prokopat ven - <sup>Emi</sup> zkouší se <sup>A</sup> prokopat ven!<br>zkouším se prokopat ven - zkouší se prokopat ven!<br>zkouší se prokopat ven!<br><br>o půl třetí na náměstí ve Valašském Meziříčí<br>jdu co noha nohu mine a každý sám sobě jsme stínem<br>nic mi není přitom melu z posledního<br>těžko říct o tom něco konkrétního<br>stejná slova kolem stejný tváře<br>a každý sám sobě jsme lhářem a já už zase<br><br>Ref:",
    "youtube": "2GRxd6cuV4E",
    "supermusic": "53",
    "id": "013d4bcc-1af7-404d-ad72-710f08d1b789",
    "author": "Mňága A Žďorp",
    "title": "Made in Valmez"
  },
  {
    "content": " <sup>C</sup> Když je v Praze abnormální hic, chodím k vodě až do Měche <sup>A7</sup> nic,<br> <sup>Dmi</sup> Měchenická <sup>G7</sup> plovárna má <sup>C</sup> krásné oko <sup>A7</sup> lí,<br> <sup>D7</sup> jěště krásnější než bazén <sup>G7</sup> v Praze 4 Podolí <sup>C</sup> .<br><br> <sup>C</sup> Má milá tam se mnou chodívá, chleba s máslem sebou nosí <sup>A7</sup> vá,<br>a když se jí <sup>Fmi</sup> šipka zdaří <sup>C</sup> úsměv na rtech má <sup>A7</sup> ,<br> <sup>Dmi</sup> chleba s másle <sup>G7</sup> m ukousnout mi <sup>C</sup> dá.<br><br>Když je v Praze abnormální mráz, do Měchenic táhne mě to zas,<br>do prázdného bazénu tam s milou chodíme, na betonu někde na dně do očí si hledíme,<br><br>Neplavou tam žádné rybičky, dáváme si sladké hubičky,<br>má milá je v dobré mířě, úsměv na rtech má, chleba s máslem ukousnout mi dá. ",
    "youtube": "8QCcnznTwas",
    "supermusic": "47528",
    "id": "f932db85-85a1-41eb-b5d2-8e3fae7b64b0",
    "author": "Ivan Mládek",
    "title": "Když je v Praze hic"
  },
  {
    "content": " <sup>Emi</sup> V noci někdy nespím a <sup>Ami</sup> civím do zahrady<br> <sup>D</sup> Se zástupem lidí kolem <sup>Hmi</sup> nevim si už rady<br> <sup>G</sup> Asi spadnul řemen <sup>C</sup> do převodu total<br> <sup>Ami</sup> Delirium tremens <sup>Hmi</sup> a já se tak rád motal<br>Doktor Josef Goebbels na terasu pajdá<br>A na vaně jsou bílý myši mezi nima Aida<br>Možná Caligula přijde s Messalinou<br>Doufám, že se zula a nepoškrábe lino<br><br>Ref.: Je tady <sup>G</sup> Čingischán Rumcajs i <sup>C</sup> Cipísek<br>Koktejl <sup>D</sup> je namíchán a myši diví se<br>Je konec <sup>G</sup> pásmu dní, ráno už <sup>C</sup> nespatřím<br>Prstýnek <sup>D</sup> zásnubní na můj prst nepatří<br>Vanu si napouštím a skočím za Aidou<br>vylezu na poušti, ráno mě nenajdou<br> <sup>Ami</sup> <sup>Hmi</sup> <sup>Emi</sup> <br><br>Cinká moje tramvaj a stydne ranní káva<br>A na dveře marně buší domácí, ta kráva<br>Policie hledá, nenachází stopy<br>Moje dívka bledá zatím sbírá zátky od piv<br>Asi něco tuší, vždyť ta holka není hloupá<br>Právě jsem se narodil a pára k nebi stoupá<br>Možná budu písař anebo lámat kámen<br>Sedmnáctý císař na hradbách zpívat Ámen<br><br>Ref:",
    "youtube": "Sy-WuK78rNw",
    "supermusic": "159858",
    "id": "3778ff5c-1526-48d8-b631-50182808f1d8",
    "author": "Tři Sestry",
    "title": "Aida"
  },
  {
    "content": " <sup>Ami</sup> Dneska už mě <sup>C</sup> fóry ňák <sup>Ami</sup> nejdou přes <sup>E7</sup> pysky<br> <sup>Ami</sup> Stojím s dlouhou <sup>C</sup> kravatou na <sup>Ami</sup> bedně <sup>E7</sup> vod whis <sup>Ami</sup> ky.<br>Stojím s dlouhým <sup>C</sup> vobojkem jak <sup>Ami</sup> stájovej <sup>E7</sup> pinč.<br> <sup>Ami</sup> Tu kravatu <sup>C</sup> co nosím mi <sup>Ami</sup> navlík <sup>E7</sup> soudce <sup>Ami</sup> Linč.<br><br>Ref: <sup>A</sup> Tak kopni do tý <sup>D</sup> bedny ať <sup>E</sup> panstvo ne <sup>A</sup> čeká<br>jsou dlouhý schody do <sup>D</sup> nebe a štre <sup>E</sup> ka daleká <sup>A</sup> .<br>Do nebeskýho <sup>D</sup> báru já su <sup>E</sup> cho v kr <sup>A</sup> ku mám.<br>Tak kopni do tý bed <sup>D</sup> ny ať <sup>E</sup> na cestu se <sup>A</sup> dám.<br><br>Mít tak všechny brdny od whisky vypitý.<br>Postavil bych malej dům na louce ukrytý.<br>Postavil bych malej dům a z okna koukal ven<br>a chlastal bych tam s Billem a chlastal by tam Ben.<br><br>Kdyby si se hochu jen pořád nechtěl rvát.<br>Nemusel jsi dneska na týhle bedně stát.<br>Moh' si někde v suchu tu svoji whisky pít<br>nemusel si hochu na krku laso mít.<br><br>Když jsem štípnul koně a ujel jen pár mil<br>nechtěl běžet dokavád se whisky nenapil<br>zatracená smůla zlá a zatracenej pech<br>když kůň cucá whisku jak u potoka mech.<br><br>Až kopneš do tý bedny, jak se to dělává<br>do krku ti zůstane jen dírka mrňavá.<br>Jenom dírka mrňavá a k smrti jenom krok.<br>mám to smutnej konec - a whisky ani lok",
    "youtube": "hn00oBo0ZvU",
    "supermusic": "48895",
    "id": "9f3819a8-a47a-4dc6-804f-3858219aedec",
    "author": "Lidovky",
    "title": "Bedna od whisky"
  },
  {
    "content": " <sup>D</sup> Nad stádem <sup>A</sup> koní <sup>Emi</sup> <br> <sup>Emi</sup> podkovy <sup>G</sup> zvoní, zvo <sup>D</sup> ní<br> <sup>D</sup> Černý vůz <sup>A</sup> vlečou <sup>Emi</sup> <br> <sup>Emi</sup> a slzy <sup>G</sup> tečou a já <sup>D</sup> volám<br><br>Tak neplač můj kamaráde<br>náhoda je blbec, když krade<br>Je tuhý jak veka a řeka ho splaví<br>Máme ho rádi<br>no tak <sup>C</sup> co, tak <sup>G</sup> co, tak <sup>A</sup> co<br><br>Vždycky si přál až bude popel<br>i s kytarou, hou<br>Vodou ať plavou, jen žádný hotel<br>s křížkem nad hlavou<br><br>Až najdeš místo<br>kde je ten pramen a kámen co praská<br>Budeš mít jisto<br>patří sem popel a každá láska<br>No tak <sup>C</sup> co, tak <sup>G</sup> co, tak <sup>A</sup> co<br><br>Nad stádem koní<br>podkovy zvoní, zvoní<br>Černý vůz vlečou<br>a slzy tečou a já šeptám<br><br>Vysyp ten popel kamaráde<br>do bílé vody, vody<br>vyhasnul kotel<br>a náhoda je<br>štěstí od podkovy",
    "youtube": "4ksA3xxGOtU",
    "supermusic": "938319",
    "id": "c0729c69-417a-42c9-ae5b-d7b01a604a7c",
    "author": "Buty",
    "title": "Nad stádem koní"
  },
  {
    "content": " <sup>Ami</sup> Jedu takhle tábořit <sup>E</sup> Škodou 100 na <sup>Ami</sup> Oravu,<br>spěchám proto, riskuji, pro <sup>E</sup> jíždím přes Mo <sup>Ami</sup> ravu.<br> <sup>G7</sup> Řádí tam to stra <sup>C</sup> šidlo, <sup>G7</sup> vystupuje z ba <sup>C</sup> žin <sup>E</sup> ,<br> <sup>Ami</sup> žere hlavně Pražáky a <sup>E</sup> jmenuje se <sup>Ami</sup> Jožin. <sup>G7</sup> <br><br>Ref: <sup>C</sup> Jožin z bažin močálem se plí <sup>G7</sup> ží,<br>Jožin z bažin k vesnici se blí <sup>C</sup> ží,<br>Jožin z bažin už si zuby brou <sup>G7</sup> sí,<br>Jožin z bažin kouše, saje, rdou <sup>C</sup> sí.<br>Na <sup>F</sup> Jožina z <sup>C</sup> bažin, <sup>G</sup> koho by to napadlo,<sup>C<br>F</sup>platí jen a <sup>C</sup> pouze práš <sup>G</sup> kovací letadlo.<sup>C, E</sup><br><br>Projížděl jsem Moravou směrem na Vizovice,<br>přivítal mě předseda, řek' mi u slivovice:<br>Živého či mrtvého Jožina kdo přivede,<br>tomu já dám za ženu dceru a půl JZD.<br><br>Ref:<br><br>Říkám dej mi, předsedo, letadlo a prášek,<br>Jožina ti přivedu, nevidím v tom háček.<br>Předseda mi vyhověl, ráno jsem se vznesl,<br>na Jožina z letadla prášek pěkně klesl.<br><br>Ref: Jožin z bažin už je celý bílý,<br>Jožin z bažin z močálu ven pílí,<br>Jožin z bažin dostal se na kámen,<br>Jožin z bažin - tady je s ním amen!<br>Jožina jsem dostal, už ho držím, johoho,<br>dobré každé lóve, prodám já ho do ZOO. ",
    "youtube": "S3tG1X5ewAg",
    "supermusic": "724",
    "id": "ec2bf9ba-30d7-4ecd-aad7-0c763d5432c0",
    "author": "Ivan Mládek",
    "title": "Jóžin z bážin"
  },
  {
    "content": " <sup>Ami</sup> Milá, tichá, <sup>F</sup> super pichá <sup>Ami</sup> infúzie <sup>F</sup> injekcie<br> <sup>Ami</sup> Keď je pri mne <sup>F</sup> znova dýcham, <sup>Em</sup> keď ju cítim, <sup>G</sup> tak rád žijem<br> <sup>Ami</sup> Od jej čaju <sup>F</sup> zrazu majú <sup>Ami</sup> starčekovia <sup>F</sup> erekcie<br> <sup>Ami</sup> Hojí lieči, <sup>F</sup> dáva klystír, <sup>Em</sup> modlia sa k nej <sup>G</sup> ateisti<br><br>Ref: Sestrič <sup>C</sup> ka z Kramá <sup>G</sup> rov, sen všet <sup>Ami</sup> kých klamá <sup>Emi</sup> rov,<br> <sup>F</sup> Ktorí jej <sup>Emi</sup> sľubujú <sup>Dm7</sup> modré z ne <sup>G</sup> ba<br>S úsme <sup>C</sup> vom, bez re <sup>G</sup> čí za pár <sup>Ami</sup> dní vylie <sup>Emi</sup> či<br> <sup>F</sup> Láska vždy <sup>Emi</sup> vylieči <sup>Dm7</sup> to, čo tre <sup>G</sup> ba<br><br>Usmievavá, modro-biela, podobá sa na anjela<br>Vždy, keď na ňu rukou myslím, nestačia mi dávať kyslík<br>Vážne neviem, čo si počnem, zdá sa mi, že nemám nárok,<br>Keď sa pri nás zjaví v nočnej, kolabuje celé ÁRO<br><br>Ref:<br><br>Angína pectoris, šepkajú doktori<br>Srdce mám na mraky, škoda debát<br>Ty nežná potvora, až ma raz otvoria<br>Tak zistia príčinu – mám to z teba<br><br>Ref:<br>Ref:<br>",
    "youtube": "3Ags7KH32HQ",
    "supermusic": "440",
    "id": "aa27e6ae-74b9-4edd-9dad-22aef6cb527d",
    "author": "Elán",
    "title": "Sestrička z Kramárov"
  },
  {
    "content": "<sup>A</sup>Na vodu  už <sup>E7</sup>jezdím jenom s <sup>A</sup>Vendou, s <sup>E7</sup>Vendou,<br>  <sup>A</sup>do kanoe <sup>E7</sup>nevlezu už s <sup>A</sup>Bendou,  s <sup>(E7)</sup>Bendou,<br>  <sup>Hmi</sup>Jenda <sup>E7</sup>Benda <sup>A</sup>nemožný je <sup>F#7</sup>zadák,<br>  <sup>H7</sup>nemá vlohy a je <sup>E7</sup>laj-, laj-, lajdák,<br>  <br>  Von <sup>A</sup>ví, že šumí les, že <sup>F#7</sup>kvete bílý bez, že <sup>H7</sup>v dáli  hárá pes, že <sup>E7</sup>vysouší se mez<br>  a že <sup>A</sup>mostem cloumá rez, že <sup>F#7</sup>říčka jde skrz ves, ale <sup>H7</sup>nevšimne si, že se blíží <sup>F7</sup>jez, <sup>E7</sup>jez, <sup>A</sup>jez.<br><br>  Jel jsem tuhle Ohři s Jendou Bendou, Bendou,<br>  proč já, houska, nejel radši s Vendou, Vendou,<br>  Jenda Benda sjel na vodu mělkou, spáchal v lodi díru vel-, vel-, velkou,<br>  Já měl náladu zlou, von zničil keňu mou,<br>  už nikdy, houpy hou, s ní nepopluji mhou,<br>  tedy šetřím na novou, na laminátovou,<br>  už ji chci mít v létě na dovolenou, -nou, -nou.<br>      ",
    "youtube": "QiYEcW_g-AY",
    "supermusic": "1161",
    "id": "31fff88e-fa85-4986-89a7-6a070e6c119d",
    "author": "Ivan Mládek",
    "title": "Jez"
  },
  {
    "content": " <sup>Dmi</sup> Když mě brali za vo <sup>F</sup> jáka<br> <sup>C</sup> stříhali mě doho <sup>F</sup> la.<br> <sup>Bb</sup> Vypadal jsem jako <sup>Dmi</sup> blbec<br> <sup>A</sup> Jak ti všichni doko <sup>Bb</sup> la <sup>C</sup> la <sup>F</sup> la <sup>A7</sup> la,<br> <sup>Dmi</sup> jak i všichni <sup>A</sup> doko <sup>Dmi</sup> la <sup>A</sup> <sup>Dmi</sup> <sup>A</sup> .<br><br>Zavřeli mě do kasáren,<br>začali mě učiti<br>jak mám správný voják býti<br>a svou zemi chrániti.<br><br>Na pokoji po večerce<br>ke zdi jsem se přitulil.<br>Vzpomněl jsem si na svou milou<br>krásně jsem si zabulil.<br><br>Když přijela po půl roce,<br>měl jsem zrovna zápal plic,<br>po chodbě furt někdo chodil,<br>tak nebylo z toho nic<br><br>Major nosí velkou hvězdu,<br>před branou ho potkala.<br>Řek jí, že má zrovna volný kvartýr<br>Tak se sbalit nechala.<br><br>Co je komu do vojáka,<br>když ho holka zradila.<br>Na shledanou, pane Fráňo Šrámku,<br>písnička už skonči <sup>Bb</sup> la <sup>C</sup> la, <sup>F</sup> la, <sup>A7</sup> la,<br> <sup>Dmi</sup> jakpak se Vám <sup>A</sup> líbi <sup>Bb</sup> la <sup>C</sup> la <sup>F</sup> la <sup>A7</sup> la?<br>no <sup>Dmi</sup> nic moc extra <sup>A</sup> neby <sup>Dmi</sup> la ..",
    "youtube": "y-mintvBH8M",
    "supermusic": "114762",
    "id": "54e4e685-15e2-4d04-bf53-549898ade506",
    "author": "Jaromír Nohavica",
    "title": "Když mě brali za vojáka"
  },
  {
    "content": "<sup>C</sup>Na Pankráci, na malém <sup>F</sup>kopečku, <sup>C</sup>stojí pěkné stromořa<sup>G</sup>dí,<br><sup>C</sup>na Pankráci, na malém <sup>F</sup>kopečku, <sup>C</sup>stojí pěkné stromořa<sup>G</sup>dí.<br><br>Měl jsem holku <sup>F</sup>namluveCnou, ale jiný mi <sup>G</sup>chodí za <sup>C</sup>ní,<br><sup>G</sup>měl jsem holku <sup>F</sup>namluvenou, ale jiný mi <sup>G</sup>chodí za <sup>C</sup>ní.<br><br>Vy mládenci, kteří jste jako já, nemilujte doopravdy,<br>vy mládenci, kteří jste jako já, nemilujte doopravdy.<br><br>Pomilujte, pošpásujte, ale lásku jim neslibujte,<br>pomilujte, pošpásujte, ale lásku jim neslibujte.<br><br>Zdálo se mi za štvrtka na pátek, že jsem se svou milenkou spal,<br>zdálo se mi za štvrtka na pátek, že jsem se svou milenkou spal.<br><br>Probudím se, obrátím se, ale jak mně ten sen oklamal,<br>probudím se, obrátím se, ale jak mně ten sen oklamal.",
    "youtube": "qbCFXalk3eo",
    "supermusic": "173471",
    "id": "03e02d76-b382-4fdd-b92c-a765c119d0e3",
    "author": "Lidovky",
    "title": "Na Pankráci"
  },
  {
    "content": "Tvůj <sup>C</sup> krok je lehký jako <sup>Emi</sup> dech,<br>kudy <sup>F</sup> jdeš, tam začíná <sup>G</sup> bál,<br>šaty <sup>C</sup> tvé, ty šila tvá <sup>Emi</sup> máma,<br>nemáš <sup>F</sup> prsten, kdo by ti jej <sup>G</sup> dal, nevím sám.<br><br>Tvým bytem je studentská kolej,<br>jedna postel, gramofon,<br>Ringo Starr se z obrázku dívá,<br>já bych snad, co vidí jen on, viděl rád.<br><br>R: Kdo v <sup>C</sup> chází do tvých snů, má <sup>Emi</sup> lásko,<br> <sup>F</sup> když nemůžeš v noci <sup>G</sup> spát,<br>komu <sup>C</sup> patří ty kroky, co <sup>Emi</sup> slýcháš,<br>a <sup>F</sup> myšlenky tvé chtěl bych <sup>G</sup> znát jedenkrát.<br><br>Co znám, to jsou známky z tvých zkoušek,<br>a vím, že máš ráda beat<br>a líbí se ti Salvátor Dalí,<br>jen pro lásku chtěla bys žít, milovat.<br><br>Pojedeš na prázdniny k moři,<br>až tam, kde slunce má chrám,<br>jeho paprskům dáš svoje tělo<br>a dál se jen věnuješ hrám milostným, a ty já znám.<br><br>Když pak napadne sníh, pojedeš do hor<br>se svou partou ze školních let,<br>přátel máš víc, než bývá zvykem,<br>ale žádný z nich nezná tvůj svět, netuší.<br><br>R: Kdo vchází do tvých snů, má lásko,<br>když nemůžeš v noci spát,<br>čí jsou ty kroky, co slýcháš,<br>a myšlenky tvé chtěl bych znát jedenkrát.<br><br>Tvůj stín, to je stín něžných písní,<br>tvá dlaň je tajemství víl,<br>kdo slýchá tvůj hlas, ten je ztracen,<br>ten se navždy polapil, úsměv tvůj, to je ta past.<br><br>Ten pán, co prý si tě vezme,<br>musí být multimilionář,<br>říkáš to všem tak vážně, že ti věří,<br>jen já stále pročítám snář a hledám v něm.<br><br>R: Kdo vchází do tvých snů, má lásko,<br>když nemůžeš v noci spát,<br>komu patří ty kroky, co slýcháš,<br>a myšlenky tvé chtěl bych znát jedenkrát.<br><br>Já tu čekám, až vrátíš se z toulek,<br>ty dálky nevedou dál,<br>ať chceš nebo nechceš, tak končí,<br>a vítr nikdo nespoutal, ani já.<br><br>Pojď jednou ke mně blíž, jedenkrát,<br>a já ti povím, kdo vlastně jsem,<br>vypni gramofon, nech té hry, zanech přátel,<br>slétni z oblak na pevnou zem, tiše stůj a poslouchej.<br><br>R: To já vcházím do tvých snů, má lásko,<br>když nemůžeš v noci spát,<br>a mé jsou ty kroky, co slýcháš,<br>jen myšlenky tvé chtěl bych znát...",
    "youtube": null,
    "supermusic": "566",
    "id": "0c389d6e-b8a0-44ed-aefc-4667466cc192",
    "author": "Václav Neckář",
    "title": "Kdo vchází do tvých snů"
  },
  {
    "content": " <sup>A</sup> Čí že ste, husličky, <sup>D</sup> či <sup>A</sup> e,<br> <sup>Hmi</sup> kdo vás tu <sup>F#mi</sup> zanech <sup>E</sup> al<br> <sup>A</sup> Čí že ste, husličky, <sup>D</sup> či <sup>A</sup> e,<br> <sup>Hmi</sup> kdo vás tu <sup>F#mi</sup> zanech <sup>E</sup> al<br> <sup>Hmi7</sup> na trávě pová <sup>A</sup> lané <sup>D</sup> ,<br> <sup>Hmi</sup> na trávě <sup>E</sup> pová <sup>A</sup> lané <sup>D</sup> <br> <sup>Hmi</sup> u paty <sup>F#mi</sup> oře <sup>E</sup> cha?<sup>Hmi, F#mi, E</sup><br><br>A kdože tu trávu tak zválal, aj modré fialy,<br>A kdože tu trávu tak zválal, aj modré fialy,<br>že ste, husličky, samé<br>že ste, husličky, samé na světě zostaly?<br><br>A který tu muzikant usnul a co sa mu přišlo zdát,<br>A který tu muzikant usnul a co sa mu přišlo zdát,<br>co sa mu enem zdálo, bože(-),<br>co sa mu enem zdálo, bože(-), že už vjec nechtěl hrát?<br><br>Zahrajte, husličky, samy, zahrajte zvesela,<br>Zahrajte, husličky, samy, zahrajte zvesela,<br>až sa tá bude trápit,<br>až sa tá bude trápit, která ho nechtěla.",
    "youtube": "ur2iYxMfUwo",
    "supermusic": "2506",
    "id": "e4b79fc1-3636-4ebb-98b6-8675df6a2b23",
    "author": "Vlasta Redl",
    "title": "Husličky"
  },
  {
    "content": " <sup>G</sup> Sbohem galá <sup>Emi</sup> nečko <sup>Ami</sup> já už musím <sup>D</sup> jí <sup>G</sup> ti <sup>A7</sup> <br> <sup>D</sup> sbohem galá <sup>Hmi</sup> nečko <sup>Emi</sup> já už musím <sup>A</sup> jíti <sup>D4sus</sup> <sup>D</sup> <br> <sup>Ami</sup> Kyselé ví <sup>D</sup> nečko, <sup>G</sup> kyselé ví <sup>Ami</sup> neč <sup>D</sup> ko<br> <sup>G</sup> Podalas mně <sup>Ami</sup> k <sup>D</sup> pi <sup>G</sup> tí<br><br>Sbohem galánečko rozlučme sa v pánu<br>Kyselé vínečko, kyselé vínečko<br>Podalas mně v džbánu.<br><br>Ač bylo kyselé přeca sem sa opil<br>Eště včil sa stydím, eště včil sa stydím<br>Co jsem všechno tropil<br><br>Ale sa nehněvám žes mňa ošidila<br>To ta moja žízeň to ta moja žízeň<br>Ta to zavinila<br>",
    "youtube": "ravx37dBHhU",
    "supermusic": "905",
    "id": "2dfc175a-51de-4b34-8f23-8f56ea3a27e7",
    "author": "Vlasta Redl",
    "title": "Sbohem galánečko"
  },
  {
    "content": " <sup>D</sup> Za niečím <sup>Ami7</sup> krásnym<br> <sup>D</sup> tak rýchlo <sup>Ami7</sup> ako sa dá<br> <sup>D</sup> do tvojich <sup>Ami7</sup> básní<br> <sup>G</sup> márne rým hľa <sup>D</sup> dám<br>Za niečím zvláštnym<br>do záhrad kde nie je kríž<br>kam sa to blázniš<br>nestíham s tebou ísť<br><br>Ref: <sup>D</sup> Spomaľ máš privy <sup>G</sup> sokú rýchlosť<br>vnímam ju len <sup>D</sup> šiestym zmyslom<br> <sup>D</sup> mááá <sup>G</sup> m závrat<br> <sup>D</sup> náhlim sa po <sup>G</sup> tvojom boku<br>a mám už dosť zbytočných pokút<br> <sup>D</sup> má <sup>G</sup> m sa báť?<br><br>Za niečím stálym<br>tak rýchlo ako to dá<br>sa všetko máli<br>aj napriek výhodám<br>Za niečím bájnym<br>do záhrad kde nie je kríž<br>kam sa to blázniš<br>nestíham s tebou ísť ",
    "youtube": "YyfjLCajbCw",
    "supermusic": "680893",
    "id": "b0f77e95-f994-4d98-9b19-7468976748fa",
    "author": "Peha",
    "title": "Spomaľ"
  },
  {
    "content": "<sup>Ami</sup> Větr sněh <sup>A2</sup> zanésl z <sup>Ami</sup> hor do <sup>A2</sup> polí,<br><sup>Ami</sup> já idu <sup>C</sup> přes kopce, <sup>G</sup> přes údolí <sup>Ami</sup> ,<br><sup>C</sup> idu k tvej <sup>G</sup> dědině zatúla <sup>C</sup> nej,<br><sup>F</sup> cestičky sně <sup>c</sup> hem sú <sup>E</sup> zafúkané.<sup>Ami</sup>  <sup>F7maj</sup> <sup>Ami</sup> <sup>E4sus</sup><br><br>R:  <sup>Ami</sup> Zafúkané<sup>C</sup> , <sup>G</sup> zafúkané <sup>C</sup><br><sup>F</sup> kolem mňa vše <sup>C</sup> cko je <sup>Dmi</sup> zafúkané <sup>E</sup> <br><sup>Ami</sup> Zafúkané <sup>C</sup> , <sup>G</sup> zafúkané <sup>C</sup> ,<br><sup>F</sup> kolem mňa <sup> C</sup> fšecko je <sup>E</sup> zafúkané <sup>Ami</sup>  <sup>Emi</sup> <sup>D</sup> <sup>G</sup> <sup>H7</sup> <sup>Emi</sup> <sup>D</sup> <sup>G</sup> <sup>H7</sup> <sup>Emi</sup><br><br>Už vašu chalupu z dálky vidím,<br>srdce sa ozvalo, bit ho slyším,<br>snáď enom pár kroků mi zostává,<br>a budu u tvého okénka stát.<br><br>R: /: Ale zafúkané, zafúkané,<br>okénko k tobě je zafúkané. :/<br><br>Od tvého okna sa smutný vracám,<br>v závějoch zpátky dom cestu hledám,<br>spadl sněh na srdce zatúlané,<br>aj na mé stopy - sú zafúkané.<br><br>R.: /: Zafúkané, zafúkané,<br>mé stopy k tobě sú zafúkané. :/ ",
    "youtube": "QU76aTiFcr8",
    "supermusic": "168516",
    "id": "fa3e9d97-47d5-4f3d-82f6-e04052a2a644",
    "author": "Fleret",
    "title": "Zafúkané"
  },
  {
    "content": " <sup>A</sup> V patnácti s prvním cigárem<br> <sup>H</sup> Říkal jsem si aspoň trochu zestárnem<br>Z plnejch plic <sup>D</sup> <br>A pak už se mnou nebylo nic <sup>A</sup> <br>Tak jsem to zkusil s novou motorkou<br>S Yesterday a Žlutou ponorkou<br>Řekla dík a pak si mávla na taxík<br><br> <sup>F#mi</sup> Kdekdo by chtěl <sup>D</sup> lítat ke hvězdám<sup>A<br>F#mi</sup>Každej na to <sup>D</sup> musí přijít sám <sup>E</sup> <br>Jen <sup>D/F#</sup>sám, jen <sup>G</sup> sám, jen <sup>E/G#</sup> sám<br><br>Ref: <sup>A</sup> Na ptáky jsme krá <sup>E</sup> tký<br> <sup>F#mi</sup> Na ptáky jsme <sup>D</sup> malý páky<br> <sup>A</sup> Je to tak to <sup>E</sup> my si dáme na zobák <sup>A</sup> <br><br>První metr piv k tomu velkej rum<br>A druhej den věděl celej dům<br>Co jsem pil a že jsem se s tím nemazlil<br>Povídám půjdem do parku<br>Na letním nebi ukážu ti Polárku<br>Řekla dík ale snad abych ji radši svlík<br><br>Ref:",
    "youtube": "gIoNNPkdtnY",
    "supermusic": "2777",
    "id": "370f9295-5f50-4d62-8f0b-e3446b833db1",
    "author": "Janek Ledecký",
    "title": "Na ptáky jsme krátký"
  },
  {
    "content": "<sup>Dmi</sup>Ostravo, Ostra<sup>A7</sup>vo, <br>město mezi městy, <sup>Dmi</sup>hořké <sup>A7</sup>moje štěstí. <br><sup>Dmi</sup>Ostravo, Ostra<sup>A7</sup>vo, <br>černá hvězdo nad hla<sup>Dmi</sup>vou. <br><br><sup>C</sup>Pán Bůh rozdal jiným <sup>F</sup>městům všecku krásu, <br><sup>Gmi</sup>parníky na řekách a <sup>A7</sup>dámy všité do atlasu. <br><sup>Dmi</sup>Ostravo - srdce ru<sup>A7</sup>dé, <br>zpečetěný osu<sup>Dmi</sup>de.<sup>A7,Dmi</sup><br><br><sup>Dmi</sup>Ostravo, Ostra<sup>A7</sup>vo, <br>kde jsem oči nechal, <sup>Dmi</sup>když jsem <sup>A7</sup>k tobě spěchal. <br><sup>Dmi</sup>Ostravo, Ostra<sup>A7</sup>vo, <br>černá hvězdo nad hla<sup>Dmi</sup>vou. <br><br><sup>C</sup>Ať mě moje nohy <sup>F</sup>nesly, kam mě nesly, <br><sup>Gmi</sup>ptáci na obloze <sup>A7</sup>jenom jednu cestu kreslí. <br><sup>Dmi</sup>Ostravo - srdce ru<sup>A7</sup>dé, <br>zpečetěný osu<sup>Dmi</sup>de.<sup>A7,Dmi</sup>",
    "youtube": "11cBG5Oo1lE",
    "supermusic": "44542",
    "id": "fc563f49-51ea-4f8e-8f8e-3f8b1b29c0b3",
    "author": "Jaromír Nohavica",
    "title": "Ostravo, Ostravo"
  },
  {
    "content": "Ref: |: <sup>C</sup> Pijte vodu, pijte pitnou vodu, pijte vodu a <sup>G</sup> nepijte <sup>C</sup> rum :|<br><br> <sup>C</sup> Jeden smutný ajznboňák pil na pátém nástupišti <sup>G</sup> ajerko <sup>C</sup> ňak.<br> <sup>C</sup> Huba se mu slepila a diesellokomotiva ho <sup>G</sup> zabi <sup>C</sup> la.<br>Ref:<br><br>V rodině u Becherů pijou becherovku přímo ze džberů.<br>Proto všichni Becheři mají trable s játrama a páteří.<br>Ref:<br><br>Pil som vodku značky Gorbačov a potom povedal som všeličo a voľačo.<br>Vyfásol som za to tri roky, teraz pijem chlórované patoky.<br>Ref:<br><br>My jestežny chlopci z Warszawy, chodime pociungem za robotou do Ostravy.<br>Štery litry vodky i mnužstvo piv, bardzo fajny kolektiv.<br>Ref:<br><br>Jedna paní v Americe ztrapnila se převelice.<br>Vypila na ex rum, poblila jim Bílý dům.<br>Ref:",
    "youtube": "Ukdnh7ZgzeM",
    "supermusic": "575",
    "id": "3ccae1a3-3839-4434-9b27-6d1e9ee18608",
    "author": "Jaromír Nohavica",
    "title": "Pijte vodu"
  },
  {
    "content": " <sup>G</sup> <sup>C</sup> <sup>D</sup> <br> <sup>G</sup> Toulám se <sup>D</sup> ulicí a <sup>F</sup> ranní dlažba <sup>C</sup> studí<br> <sup>G</sup> Někdy si <sup>D</sup> připadám jak <sup>F</sup> ohroženej <sup>C</sup> druh<br> <sup>G</sup> Poslední <sup>D</sup> dobou noční <sup>F</sup> pitky trochu <sup>C</sup> nudí<br>svítá <sup>D</sup> dřív a těžkej je vzduch<br><br>Sundej si šaty nečekej čas rychle letí<br>Cítíš jak z nebe padá na hlavy nám sůl<br>To PánBůh pláče pro svý opuštěný děti<br>Všeho moc a stejně jen půl<br><br>Ref: Tak radši <sup>G</sup> udělej mi<br> <sup>Ami</sup> to co mám <sup>C</sup> rád co se mi <sup>D</sup> líbí<br>Udělej mi to co mám rád teď najednou<br>Udělej mi to co mám rád a já ti slibím<br>že už <sup>G</sup> nikdy s jinou <sup>D</sup> nebudu <sup>C</sup> spát<br><br>Vedle mě ležíš postel saje zbytek vína<br>Na stropě bizón kterej časem opadá<br>Stačilo říct že si zvláštní, trochu jiná<br>hloupý kecy, tak se mi zdá<br><br>Ref:<br><br>Na skále stojím jako blázen v teplým dešti<br>A chtěl bych zpátky všechny rány co jsem dal<br>Málo se bojím stačí krok a můžu letět<br>Někam pryč a ještě kus dál<br><br>Ref:",
    "youtube": "8fjX9R0e09Q",
    "supermusic": "3515",
    "id": "d6b11334-e9b5-400b-b7e0-21ddd2c36db9",
    "author": "Kabát",
    "title": "Ohroženej druh"
  },
  {
    "content": " <sup>G</sup> Mladičká básnířka <sup>Hmi</sup> s korálky nad kotníky <sup>Emi</sup> <sup>D</sup> <br> <sup>G</sup> Bouchala na dvířka <sup>Hmi</sup> paláce poetiky <sup>Emi</sup> <sup>D</sup> <br>S někým se <sup>G</sup> vyspala, někomu <sup>D</sup> nedala, láska jako <sup>Emi</sup> hobby<br> <sup>Cmi</sup> pak o tom napsala <sup>D</sup> blues na čtyři <sup>G</sup> doby<br><br>Své srdce skloňovala podle vzoru Ferlinghetti<br>Ve vzduchu nechávala viset vždy jen půlku věty<br>Plná tragiky, plná mystiky, plná splínu<br>Tak jí to otiskli v jednom magazínu<br><br>Bývala viděna v malém baru u Rozhlasu<br>od sebe kolena a cizí ruka kolem pasu<br>Trochu se napila, trochu se opila na účet redaktora<br>Za týden na to byla hvězdou mikrofóra<br><br>Pod paží nosila rozepsané rukopisy<br>Ráno se budila vedle záchodové mísy<br>Múzou políbena, životem potřísněná, plná zázraků<br>A pak ji vyhodili z gymplu a hned na to i z baráku<br><br>Šly řeči okolím, že měla něco se esenbáky<br>Ať bylo cokoliv, přestala věřit na zázraky<br>Cítila u srdce, jak po ní přešla železná bota<br>Tak o tom napsala sonet a ten byl ze života ",
    "youtube": "iTTlE43LUNc",
    "supermusic": "350248",
    "id": "96b7b5df-90d4-4de3-a48b-0f5a5955aef2",
    "author": "Jaromír Nohavica",
    "title": "Básnířka"
  },
  {
    "content": " <sup>G</sup> Skončili jsme jasná zpráva,<br> <sup>Emi</sup> proč o tebe <sup>C</sup> zakopávam <sup>D</sup> dál,<br> <sup>Ami</sup> projít bytem <sup>C</sup> já abych se <sup>G</sup> bál.<br><br> <sup>G</sup> Dík tobě se vidím zvenčí,<br> <sup>Emi</sup> připadám si <sup>C</sup> starší menší <sup>D</sup> sám,<br> <sup>Ami</sup> kam se kouknu, <sup>C</sup> kousek tebe <sup>G</sup> mám.<br><br> <sup>Emi</sup> Pěnu s vůni <sup>Hmi</sup> jablečnou, <sup>Emi</sup> vyvanulý <sup>Hmi</sup> sprej,<br> <sup>Emi</sup> telefon, cos <sup>G</sup> ustřihla mu <sup>D</sup> šňůru,<br> <sup>Emi</sup> knížku krásně <sup>Hmi</sup> zbytečnou, <sup>Emi</sup> co má lživý <sup>Hmi</sup> děj,<br> <sup>Emi</sup> píše se v ní, <sup>G</sup> jak se lítá <sup>D</sup> vzhůru,<br>lítá <sup>Ami</sup> vzhůru, ve dvou <sup>D</sup> vzhůru.<br><br>Odešla's mi před úsvitem,<br>mám snad bloudit vlastním bytem sám,<br>kam se kouknu, kousek tebe mám.<br><br>Ref: <sup>G</sup> Skončili jsme jasná zpráva,<br> <sup>Emi</sup> není komu z <sup>C</sup> okna mávat <sup>D</sup> víc,<br> <sup>Ami</sup> jasná zpráva, <sup>C</sup> rub, co nemá <sup>G</sup> líc. ",
    "youtube": "iZhLqDh-lVY",
    "supermusic": "385873",
    "id": "cf795cf1-ec82-4e08-aca0-22a8054a3bd9",
    "author": "Olympic",
    "title": "Jasná zpráva"
  },
  {
    "content": " <sup>D</sup> Nečekala na nic a nebyla <sup>G</sup> hloupá a <sup>A</sup> já jsem ji výjimečně do voka <sup>D</sup> pad<br> <sup>D</sup> najednou koukám jak se život <sup>G</sup> houpá <sup>A</sup> a nikdo neví, co se může <sup>D</sup> stát<br> <sup>A</sup> pojedeme spolu někam na pro <sup>D</sup> cházku, <sup>A</sup> probereme život hezky ze všech <sup>D</sup> stran<br> <sup>A</sup> já budu hvězda jako na <sup>D</sup> obrázku <sup>A</sup> a vona se mnou pěkně <sup>G</sup> zamá <sup>A</sup> vá<br><br>Ref: <sup>A</sup> Je totiž <sup>D</sup> náruži <sup>A</sup> vá a <sup>G</sup> nekouká co kolem <sup>D</sup> lítá<br> <sup>D</sup> život uží <sup>A</sup> vá, <sup>G</sup> tak jak to umí a <sup>G</sup> jak se jí to líbí. <sup>A</sup> <br><br>Vo jiný věci  už nemám zájem musím ji vidět ještě dnes<br>Vona bude moje cesta rájem a já budu její žlutej pes<br>Není to žádná laciná holka a kdyby chtěla nudit ani neví jak<br>já bych ji pořád jenom líbal, vona to vidí taky tak.<br><br>Ref:",
    "youtube": "igMTzxTM7Jc",
    "supermusic": "805",
    "id": "074433a5-6c28-4bdc-9a90-79f91595590e",
    "author": "Žlutý Pes",
    "title": "Náruživá"
  },
  {
    "content": "Z <sup>G</sup> kraje týdne málo jsem <sup>C</sup> ti <sup>G</sup> vhod<br>ve středu pak ztrácíš ke mně <sup>C</sup> kód<br> <sup>Ami</sup> Sedmý den jsem s tebou i když <sup>D</sup> sám<br>Osmý den <sup>G</sup> schází nám<sup>C,G,C</sup><br><br>V pondělí máš důvod k mlčení<br>Ve středu mně pláčem odměníš<br>V neděli už nevím, že tě mám<br>Osmý den schází nám<br><br>Ref: <sup>D</sup> Tužku nemít, <sup>C</sup> nic mi <sup>G</sup> nepovíš<br> <sup>D</sup> Řekni prosím <sup>C</sup> aspoň <sup>G</sup> přísloví<br> <sup>D</sup> Osmý den je <sup>C</sup> nutný - já to <sup>Ami</sup> vím<br> <sup>C</sup> Sedm nocí <sup>Ami</sup> spíš, jen <sup>G</sup> spíš, vždy spíš.<br><br>Někdo to rád horké, jiný ne<br>Mlčky naše láska pomine<br>Jen si řeknem v duchu každý sám<br>Osmý den scházel nám.<br><br>Ref:",
    "youtube": "M2tfgF11BTE",
    "supermusic": "466",
    "id": "9cc17eda-5ec7-4b9e-b419-fd424b531313",
    "author": "Olympic",
    "title": "Osmý den"
  },
  {
    "content": " <sup>Ami</sup> /:Mezi <sup>G</sup> horami <sup>Ami</sup> <br> <sup>C</sup> lipka <sup>G</sup> zelená <sup>C</sup> :/<br> <sup>C</sup> /:Zabili Janka <sup>G</sup> Janíčka, <sup>Ami</sup> Janka <sup>Ami</sup> miesto <sup>Emi</sup> jele <sup>Ami</sup> ňa:/<br><br>Keď ho zabili,<br>zamordovali,<br>na jeho hrobě, na jeho hrobě, kříž postavili.<br><br>Ej, křížu, křížu,<br>ukřižovaný,<br>Zde leží Janík, Janíček, Janík, zamordovaný.<br><br>Tu šla Anička,<br>plakat Janíčka,<br>hneď na hrob padla, a viac nevstala, dobrá Anička.",
    "youtube": "o7I41YWDoKs",
    "supermusic": "185587",
    "id": "dd87f0cb-717d-45ea-a1ae-2c19536cb780",
    "author": "Lidovky",
    "title": "Mezi horami"
  },
  {
    "content": "Býval jsem <sup>D</sup> chudý jak <sup>G</sup> kostelní <sup>D</sup> myš,<br>na půdě <sup>F#mi</sup> půdy jsem <sup>Hmi</sup> míval svou <sup>A</sup> skrýš,<br>/: <sup>G</sup> pak jednou v <sup>D</sup> létě <sup>A</sup> řek' jsem si: <sup>Hmi</sup> ať,<br> <sup>G</sup> svět facku <sup>D</sup> je tě, a <sup>G</sup> tak mu to <sup>D</sup> vrať. :/<br><br>Když mi dát nechceš, já vezmu si sám,<br>zámek jde lehce a adresu znám,<br>/: zlato jak zlato, dolar či frank,<br>tak jsem šel na to do National Bank. :/<br><br>Ref: Jdou po mně, <sup>D</sup> jdou, <sup>G</sup> jdou, <sup>D</sup> jdou,<br>na každém <sup>F#mi</sup> rohu ma <sup>Hmi</sup> jí fotku <sup>A</sup> mou,<br> <sup>G</sup> kdyby mě <sup>D</sup> chytli, <sup>A</sup> jó, byl by <sup>Hmi</sup> ring,<br> <sup>G</sup> tma jako v <sup>D</sup> pytli je v <sup>C</sup> celách Sing- <sup>G</sup> sing.<sup>A, D, G, D</sup><br><br>Ve státě Iowa byl od poldů klid,<br>chudičká vdova mi nabídla byt,<br>/: jó, byla to kráska, já měl peníze,<br>tak začla láska jak z televize. :/<br><br>Však půl roku nato řekla mi dost,<br>tobě došlo zlato, mně trpělivost,<br>/: sbal svých pár švestek a běž si, kam chceš,<br>tak jsem na cestě a chudý jak veš. :/<br><br>Ref:<br><br>Teď ve státě Utah žiju spokojen,<br>pípu jsem utáh' a straním se žen,<br>/: jó, kladou mi pasti a do pastí špek,<br>já na ně mastím, jen ať mají vztek. :/<br><br>Ref:",
    "youtube": "lkAHxN4VUuI",
    "supermusic": "82186",
    "id": "47f8ee77-034d-450f-940c-210d5f6cb3ee",
    "author": "Jaromír Nohavica",
    "title": "Jdou po mně jdou"
  },
  {
    "content": "<sup>Ami</sup>Dnes, pod okny <sup>C</sup>mými <br>prašivý <sup>G</sup>pes celou noc <sup>D</sup>vyl<br>Šel z toho <sup>Ami</sup>děs, jak mezi <sup>C</sup>svými <br>na dvoře <sup>G</sup>lez z posledních <sup>D</sup>sil<br>Tmou neslo se vití <br>hořké jak blín, černé jak bez,<br>smutné jak stín dvou živobytí<br>nahoře já, dole ten pes<br><sup>Emi</sup>Vím jak <sup>C</sup>bolí <sup>G</sup>v ráně <sup>D</sup>sůl <br><sup>Emi</sup>chléb se <sup>C</sup>solí <sup>G</sup>zlého <sup>D</sup>půl<br>Zas pod okny mými <br>prašivý pes celou noc vyl<br>smutně jak stín dvou živobytí<br>nahoře já, dole ten pes<br>Vím jak bolí...",
    "youtube": "n2VJgMChid8",
    "supermusic": "104085",
    "id": "8f8e8f1d-f731-4010-9645-6d4ad23de507",
    "author": "Doga",
    "title": "Vím jak bolí"
  },
  {
    "content": " <sup>A</sup> Míle a <sup>D</sup> míle jsou <sup>A</sup> cest kte <sup>F#mi</sup> ré znám, <sup>A</sup> jdou trá <sup>D</sup> vou i úbo <sup>E</sup> čím skal<br>Jsou cesty zpátky a jsou cesty tam a já na všech s vámi stál<br>Proč ale blátem nás kázali vést a špínou si třísnili šat<br><br>Ref: <sup>D</sup> to ví snad jen <sup>E</sup> déšť <sup>A</sup> a vítr <sup>F#mi</sup> kolem nás<br>ten <sup>D</sup> vítr co <sup>E</sup> začal prá <sup>A</sup> vě vát<br><br>Míle a míle se táhnou těch cest a zas po nich zástupy jdou<br>Kříže jsou bílé a lampičky hvězd jen váhavě svítí tmou<br>Bůh ví co růží jež dál mohly kvést, spí v hlíně těch práchnivejch blát<br><br>Dejte mi stéblo a já budu rád, i stéblo je záchranný pás<br>Dejte mi flétnu a já budu hrát a zpívat a ptát se vás<br>Proč jen se účel tak rád mění v bič a proč že se má člověk bát",
    "youtube": "UGhFjXnwApI",
    "supermusic": "10295",
    "id": "0e8fbc94-30ad-47d8-ae1b-2ddb0d047685",
    "author": "Waldemar Matuška",
    "title": "Míle a míle"
  },
  {
    "content": " <sup>A</sup> <sup>E</sup> <sup>A</sup> <sup>E</sup> <sup>A</sup> <sup>E</sup> <sup>A</sup> <sup>E</sup> <br> <sup>D</sup> Ublížil som ženám <sup>E</sup> deťom <sup>A</sup> mamám<br> <sup>D</sup> Svoju <sup>E</sup> hlbokú úctu <sup>A</sup> skladám<br> <sup>D</sup> Pred <sup>E</sup> vami moji <sup>F#mi</sup> drahí<br>Ktorým <sup>D</sup> zneuctil som <sup>E, D</sup>prahy<br> <sup>D</sup> Od <sup>E</sup> Hlohovca až do <sup>A</sup> Prahy<br><sup>D, E</sup>som <sup>A</sup> nahý<br> <sup>D</sup> Nech mi <sup>E</sup> všetci svätí pomô <sup>F#mi</sup> žu<br> <sup>D</sup> nahý <sup>E</sup> až na ko <sup>D</sup> žu<br><br>Ref: <sup>A</sup> Už <sup>C#</sup> nebudem <sup>F#mi</sup> nahý<br>Drahé <sup>D</sup> slečny drahé <sup>A</sup> ženy<br> <sup>C#</sup> Od teraz až <sup>F#mi</sup> navždy<br>zostávam <sup>D</sup> oblečený<br> <sup>A</sup> Neve <sup>C#</sup> rím že mi <sup>F#mi</sup> pomôžu <sup>H</sup> <br> <sup>D</sup> som nahý <sup>E</sup> až na ko <sup>D</sup> žu<br><br> <sup>D</sup> Nahý už som bol <sup>E</sup> <br> <sup>A</sup> stačí<br> <sup>D</sup> Normálne <sup>E</sup> na \"egy keto <sup>A</sup> három\" <sup>D, E, F#mi</sup><br> <sup>D</sup> nevyzlečiem sa <sup>E</sup> ani <sup>D</sup> pred lekárom",
    "youtube": "YDYXJ8WixBE",
    "supermusic": "5022",
    "id": "8ac38653-2628-4d4d-a068-b2c4ef91a384",
    "author": "Richard Müller",
    "title": "Nahý"
  },
  {
    "content": " <sup>Ami</sup> <sup>G</sup> <sup>Ami</sup> <sup>G</sup> <br> <sup>Ami</sup> vraj zmenila si <sup>G</sup> číslo<br> <sup>Ami</sup> a do zámku <sup>G</sup> nepasuje viac môj <sup>Fmaj7</sup> kľúč <sup>F/G</sup> <sup>F</sup> <br>Ami</sup>hlavou mi <sup>G</sup> blyslo<br> <sup>Ami</sup> asi chceš <sup>G</sup> aby som bol už <sup>Fmaj7</sup> fuč<br><br>Ref: <sup>C</sup> nebude to <sup>G</sup> také ľahké <sup>Fmaj7</sup> drahá, <sup>F/G</sup> <sup>Fmaj7</sup> <br> <sup>C</sup> mňa sa nezba <sup>G</sup> víš s tým sa lúč, <sup>Fmaj7</sup> <br> <sup>C</sup> nebude to <sup>G</sup> také ľahké <sup>Fmaj7</sup> drahá,<br> <sup>G</sup> nemôžeš ma <sup>Fmaj7</sup> vymeniť ako <sup>G</sup> rúž.<br>Som tvoj <sup>Ami</sup> muž. <sup>G</sup> <sup>Ami</sup> <sup>G</sup> <br><br>vraj koketuješ s iným<br>dolámem mu kosti, nos a sny<br>tak dopúšťaš sa viny<br>tak vrav, povedz čo máš s ním. ®: Som tvoj.<br><br>vraj trháš moje fotky<br>majetok náš rozdelí až súd<br>ja exujem veľké vodky<br>márne sa snažím zachovať kľud. ®:<br><br> <sup>F</sup> Predsa sme si <sup>G</sup> súdení<br> <sup>F</sup> ja nemôžem žiť <sup>G</sup> bez ženy<br> <sup>F</sup> ja nemôžem žiť <sup>G</sup> bez teba, to mi <sup>Ami</sup> ver.<br>A <sup>F</sup> v kostole <sup>G</sup> pred Pánom<br> <sup>F</sup> sľubovala <sup>G</sup> si nám dvom<br> <sup>F</sup> nekonečnú <sup>G</sup> lásku, vernosť, <sup>Ámi</sup>mier <sup>G</sup> mier <sup>F</sup> mier óóó..<br><br>Ref:<br><br>vraj zmenila si číslo<br>a do zámku nepasuje viac môj ",
    "youtube": "LAPtqyhydDs",
    "supermusic": "348",
    "id": "a2b53043-6f8a-4754-8711-1a658cc04071",
    "author": "Richard Müller",
    "title": "Nebude to ľahké"
  },
  {
    "content": "Close your <sup>F#mi</sup> eyes and I'll <sup>H7</sup> kiss you<br>to <sup>E</sup> morrow I'll <sup>C#mi</sup> miss you<br>re <sup>A</sup> member I'll <sup>F#mi</sup> always be <sup>D</sup> true <sup>H7</sup> <br>and then <sup>F#mi</sup> while I'm <sup>H7</sup> away<br>I'll write <sup>E</sup> home every <sup>C#mi</sup> day<br>and I'll <sup>A</sup> send all my <sup>H7</sup> loving to <sup>E</sup> you. <sup>A</sup> <sup>E</sup> <sup>H</sup> <sup>E</sup> <br><br>I'll pretend that I'm kissing<br>the lips I am missing<br>and hope that my dreams will come true<br>and then while I'm away<br>I'll write home every day<br>and I'll send all my loving to you.<br><br>Ref: All my <sup>C#mi</sup> loving <sup>C+</sup>I will send to <sup>E</sup> you<br>all my <sup>C#mi</sup> loving <sup>C+</sup>darling I'll be <sup>E</sup> true.<br>3.=1.<br>4.=2.<br>®: + all my <sup>C#mi</sup> loving all my <sup>E</sup> loving<br>all my <sup>C#mi</sup> loving I will send to <sup>E</sup> you ...",
    "youtube": "z3mWxmmsGGo",
    "supermusic": "3164",
    "id": "0f30b388-22bd-4f45-bb87-b8709de2895d",
    "author": "Beatles",
    "title": "All My Loving"
  },
  {
    "content": " <sup>D</sup> Když jsem byl malý, říkali mi naši,<br>dobře se uč a jez chytrou kaši,<br> <sup>G</sup> až jednou vyrosteš, <sup>A7</sup> budeš doktorem <sup>D</sup> práv.<br>Takový doktor sedí pěkně v suchu,<br>bere velký peníze a škrábe se v uchu,<br> <sup>G</sup> já jim ale na to řek', <sup>A7</sup> chci být hlídačem <sup>D</sup> krav.<br><br>Ref: Já chci <sup>D</sup> mít čapku s bambulí nahoře,<br>jíst kaštany a mýt se v lavoře,<br> <sup>G</sup> od rána po celý <sup>A7</sup> den zpívat si <sup>D</sup> jen,<br>zpívat si: pam pam pam <sup>D</sup> <sup>G</sup> <sup>A7</sup> <br><br>K vánocům mi kupovali hromady knih,<br>co jsem ale vědět chtěl, to nevyčet' jsem z nich:<br>nikde jsem se nedozvěděl, jak se hlídají krávy.<br>Ptal jsem se starších a ptal jsem se všech,<br>každý na mě hleděl jako na pytel blech,<br>každý se mě opatrně tázal na moje zdraví.<br><br>Ref:<br><br>Dnes už jsem starší a vím, co vím,<br>mnohé věci nemůžu a mnohé smím,<br>a když je mi velmi smutno, lehnu si do mokré trávy.<br>S nohama křížem a s rukama za hlavou<br>koukám nahoru na oblohu modravou,<br>kde se mezi mraky honí moje strakaté krávy.<br><br>Ref:",
    "youtube": "RNyj2PK0n8g",
    "supermusic": "203698",
    "id": "1a12a6f5-df3b-401f-99cd-b2985923727d",
    "author": "Jaromír Nohavica",
    "title": "Hlídač krav"
  },
  {
    "content": " <sup>G</sup> Pokaždé když tě vidím, <sup>D</sup> vím, že by to šlo<br>a když <sup>Emi</sup> jsem přemejšlel, co cítím, <sup>C</sup> tak mě napadlo<br>jestli <sup>G</sup> nechceš svýho osla vedle <sup>D</sup> mýho osla hnát<br>jestli <sup>Emi</sup> nechceš se mnou tahat <sup>C</sup> ze země rezavej drát<br><br> <sup>G</sup> Jsi loko <sup>D</sup> motiva, která <sup>Emi</sup> se řítí <sup>C</sup> tmou<br> <sup>G</sup> jsi indi <sup>D</sup> áni, kteří <sup>Emi</sup> prérií <sup>C</sup> jedou<br> <sup>G</sup> jsi kulka <sup>D</sup> vystřelená <sup>Emi</sup> do mojí <sup>C</sup> hlavy<br> <sup>G</sup> jsi prezident <sup>D</sup> a já tvé <sup>Emi</sup> spojené <sup>C</sup> státy<br><br>Přines jsem ti kytku, no co koukáš, to se má<br>je to koruna žvejkačkou ke špejli přilepená<br>a dva kelímky vod jogurtu, co je mezi nima niť<br>můžeme si takhle volat, když budeme chtít<br><br>Jsi lokomotiva, která se řítí tmou,<br>jsi indiáni, kteří prérií jedou,<br>jsi kulka vystřelená do mojí hlavy,<br>jsi prezident a já tvé spojené státy<br><br>Každej příběh má svůj konec, ale né ten náš<br>nám to bude navždy dojit, všude kam se podíváš<br>naše kachny budou zlato nosit a krmit se popcornem<br>já to každej večer spláchnu půlnočním expresem<br><br>Jsi lokomotiva, která se řítí tmou<br>jsi indiáni, kteří prérií jedou<br>jsi kulka vystřelená do mojí hlavy<br>jsi prezident a já tvé spojené státy<br><br>Dětem dáme jména Jessie, Jerry, Jad a John<br>ve stopadesáti letech ho budu mít stále jako slon<br>a ty neztratíš svoji krásu, stále štíhlá kolem pasu<br>stále dokážeš mě chytit lasem a přitáhnout na terasu ",
    "youtube": null,
    "supermusic": "935123",
    "id": "3cc35853-5753-43ca-a145-069d3771d975",
    "author": "Poletíme?",
    "title": "Lokomotiva"
  },
  {
    "content": "<sup>Ami</sup>Toužila kroužila kryla mi záda <br>souložila kouřila mě měla mě ráda <br>poháněná chtíčem vrněla jak káča <br>moje milá <sup>C</sup>plakala<sup>G</sup> <br><br><sup>Ami</sup>utíkala pryč nevěděla <br>kam pletla na mě bič já byl na to sám <br>poháněná chtíčem vrněla jak káča <br>moje milá <sup>C</sup>plakala<sup>G</sup> <br><br><sup>Ami</sup>modlila se hlásila se o svý práva <br>motala se na mý trase byla to tráva <br>poháněná chtíčem vrněla jak káča <br>moje milá <sup>C</sup>plakala<sup>G</sup> <br><br><sup>Ami</sup>moje milá <sup>C</sup>plakala<sup>G</sup> <br><sup>Ami</sup>moje milá <sup>C</sup>plakala<sup>G</sup> <br><sup>Ami</sup>moje milá <sup>C</sup>plakala<sup>G</sup> <br><sup>Ami</sup>moje milá <sup>C</sup>plakala<sup>G</sup> <br><br><sup>Ami</sup>pozdravuj <sup>C</sup>pocestný <sup>G</sup>svět <br>je malej <sup>Ami</sup>dokonalej<sup>G</sup> <br><sup>Ami</sup>pozdravuj <sup>C</sup>pocestný <sup>G</sup>svět <br>je zlej co když se <sup>Ami</sup>nevrátí<br><sup>G</sup>co když se <sup>Ami</sup>nevrátí <br>moje milá <sup>C</sup>plakala<sup>G</sup> <br>co když se <sup>Ami</sup>nevrátí ",
    "youtube": "YHEPB2xTN28",
    "supermusic": "3313",
    "id": "3f1479d8-69b4-4590-bf5f-1981794bcf4d",
    "author": "Divokej Bill",
    "title": "Plakala"
  },
  {
    "content": " <sup>H</sup> A jsem zas <sup>Gmi/A</sup>já <sup>Gmi</sup> hluboko <sup>H</sup> nad věcí<br> <sup>Cmi</sup> A jsem zas <sup>F</sup> já <sup>H</sup> pálčivá v <sup>Gmi</sup> záblescích<br>Dívam se do tebe lesklá a schýlená<br>Jsem trochu z ocele a trochu smířená<br> <sup>H</sup> A trochu <sup>Gmi7</sup> smíře <sup>A7</sup> ná…<br><br> <sup>Dmi</sup> Zasej mě v kamení, <sup>Gmi</sup> sklidíš mě v rozpuku,<br> <sup>C</sup> Trhej mě z kořenù <sup>F</sup> vezmu tě za ruku<br> <sup>Dmi</sup> Jsem tvoje nálada, <sup>Gmi</sup> jsem tvoje poslední<br> <sup>E</sup> pùlnoční zahrada <sup>A</sup> v dlážděném poledni<br>Jsem drátek ze stříbra, na noze holubí<br>Chytám tě za křídla, jsem celá naruby<br>Jsem rána bez hluku lesklá a vzdálená<br>Beru tě za ruku… <sup>A</sup> <sup>F#mi/G</sup> <sup>F#mi</sup> <sup>A</sup> <sup>Hmi</sup> <sup>E</sup> <sup>A</sup> <sup>F#MI</sup> <br><br> <sup>H</sup> A jsem zas já hluboko nad věcí<br>A jsem zas já pálčivá v záblescích<br>Dívam se do tebe, lesklá a schýlená<br>Jsem trochu z ocele a trochu smířená<br>A trochu smířená…<br><br>Zasej mě v kameních, sklidíš mě v rozpuchu,<br>Trhej mě z kořenù vezmu tě za ruku<br>Jsem tvoje nálada, jsem tvoje poslední<br>půlnoční zahrada v dlážděném poledni<br>Jsem drátek ze stříbra, na noze holubí<br>Chytám tě za křídla, jsem celá naruby<br>Jsem rána bez hluku lesklá a vzdálená<br>Beru tě za ruku …<br><br> <sup>A</sup> A jsem zas <sup>F#mi/G</sup>já <sup>F#mi</sup> hluboko <sup>A</sup> nad věcí<br> <sup>Hmi</sup> A jsem zas <sup>E</sup> já <sup>A</sup> pálčivá v <sup>F#mi</sup> záblescích<br>Hluboko nad věcí lesklá a vzdálená<br>Dívám se do sebe vidím tvá ramena<br>Vidím tvá ramena<br>",
    "youtube": null,
    "supermusic": "90",
    "id": "16ba533a-94bf-493f-af62-410c69e4216b",
    "author": "Nerez",
    "title": "Naruby"
  },
  {
    "content": "Kde si včera <sup>C</sup> bol, si <sup>E7</sup> zase ožra <sup>Ami</sup> tý<br>Si totálne na <sup>Dmi</sup> mol, to mi teraz vysve <sup>G</sup> tli<br>Kde si včera <sup>C</sup> bol, si <sup>E7</sup> zase ožra <sup>Ami</sup> tý<br>Si totálne na <sup>Dmi</sup> mol, to mi teraz vysve <sup>G</sup> tli<br><br> <sup>C</sup> Láska mám ťa rád, ja ťa <sup>E7</sup> milujem,<br> <sup>Ami</sup> Ja si ťa zoberem<br> <sup>Dmi</sup> Teraz mi neveríš, lebo mám vypité<br> <sup>G</sup> Ja si ťa zoberem<br> <sup>C</sup> Láska mám ťa rád, ja ťa <sup>E7</sup> milujem,<br> <sup>Ami</sup> Ja si ťa zoberem<br> <sup>Dmi</sup> Teraz mi neveríš, lebo mám vypité<br> <sup>G</sup> Ja si ťa zoberem",
    "youtube": null,
    "supermusic": "1383989",
    "id": "05e43709-5b05-49fe-9017-68d58ae4bd7f",
    "author": "Lidovky",
    "title": "Kde si včera bol"
  },
  {
    "content": "<sup>F#mi</sup>Vyplašený zajíc běhal<br><sup>D</sup>po polích,<br><sup>F</sup>pak nalezl <sup>E</sup>smrt v pažích<br><sup>A</sup>tetovaných.<br><sup>F#mi</sup>Kočovali ulicí i<br><sup>D</sup>náměstím,<br><sup>F</sup>asi <sup>E</sup>za štěstím,<br><sup>A</sup>či neštěstím.<br><br> R: <sup>F#mi</sup>Bulharku Jarku, tatarku z párku,<br>   <sup>D</sup>motorku z korku, Maďarku, Norku,<br>   <sup>F</sup>Japonku, Polku, <sup>E</sup>Mongolku z vdolku,<br>   <sup>A</sup>uzenou rolku, zelenou jolku.<br>   <sup>F#mi</sup>Jenom ne tuhle cikánskou holku,<br>   <sup>D</sup>jenom ne tuhle cikánskou holku,<br>   <sup>F</sup>jenom ne tuhle <sup>E</sup>cikánskou holku,<br>   <sup>A</sup>tu ne! ne!<br><br>   <sup>F#mi</sup>Ne ne ne ne ne ne ne ne <sup>D</sup>neměl jsem,<br>   <sup>F</sup>tu cikánskou <sup>E</sup>holku potkat <sup>A</sup>za lesem,<br>   <sup>F#mi</sup>ukradených slepic pírka <sup>D</sup>zdvihá dým,<br>   <sup>F</sup>potom stoupá <sup>E</sup>k nebi a já <sup>A</sup>stoupám s ním.<br><br>Já sem tvoje holka,<br>já tě mám ráda,<br>napsala mi v noci nožem<br>na záda!<br>Rozevřela se a já se<br>topil v ní,<br>jenomže ne první ani<br>poslední<br><br>R: ",
    "youtube": "vYz-i7re2Zo",
    "supermusic": "5805",
    "id": "03151219-26e7-4e9f-ae6d-eb6bc9ef62dc",
    "author": "Mig 21",
    "title": "Slepic pírka"
  },
  {
    "content": "<sup>4x A, A, G, D</sup><br><sup>A</sup>Sex je náš dělá <sup>G</sup>dobře mně i <sup>D</sup>tobě<br><sup>A</sup>Otčenáš, baby, <sup>G</sup>odříkej až v <sup>D</sup>hrobě<br><sup>A</sup>Středověk neskončil <sup>D</sup>středověk <sup>G</sup>trvá<br><sup>A</sup>Jsme černí andělé a <sup>D</sup>ty jsi byla <sup>G</sup>prvá <br><sup>A, A, G, D</sup><br><sup>A</sup>Sex je náš padaj <sup>G</sup>kapky z konců <sup>D</sup>křídel  <br><sup>A</sup>Nevnímáš zbytky <sup>G</sup>otrávených <sup>D</sup>jídel<br><sup>A</sup>Středověk neskončil <sup>D</sup>středověk <sup>G</sup>trvá<br><sup>A</sup>Jsme černí andělé a <sup>D</sup>ty jsi byla <sup>G</sup>prvá <br><sup>A,A,G,D,A</sup>A.<sup>A</sup>I.<sup>G</sup>D.<sup>D</sup><br><sup>A</sup>Sex je náš dělá <sup>G</sup>dobře mně i <sup>D</sup>tobě<br><sup>A</sup>Otčenáš, baby, <sup>G</sup>odříkej až v <sup>D</sup>hrobě<br><sup>A</sup>Ty svou víru neobrátíš, <sup>G</sup>má krev proudí v <sup>D</sup>tobě - nevíš <br><sup>A</sup>O kříž se teď neopírej, <sup>G</sup>shoříš v týhle <sup>D</sup>době - no no <br><sup>A</sup>Středověk neskončil <sup>D</sup>středověk <sup>G</sup>trvá<br><sup>A</sup>Jsme černí andělé a <sup>D</sup>ty jsi byla <sup>G</sup>prvá <br><sup>A,A,G,D</sup>A.<sup>A</sup>I.<sup>A</sup>D.<sup>G</sup>S.<sup>D</sup><br><sup>A</sup>Sex je náš padaj <sup>G</sup>kapky z konců <sup>D</sup>křídel  <br><sup>A</sup>Nevnímáš zbytky <sup>G</sup>otrávených <sup>D</sup>jídel<br><sup>A</sup>Ty svou víru neobrátíš, <sup>G</sup>má krev proudí v <sup>D</sup>tobě - nevíš <br><sup>A</sup>O kříž se teď neopírej, <sup>G</sup>shoříš v týhle <sup>D</sup>době - no no <br><sup>A</sup>Středověk neskončil <sup>G</sup>středověk <sup>D</sup>trvá<br><sup>A</sup>Jsme černí andělé a <sup>G</sup>ty jsi byla <sup>D</sup>prvá <br><sup>A</sup>Středověk neskončil <sup>G</sup>středověk <sup>D</sup>trvá<br><sup>A</sup>Jsme černí andělé a <sup>G</sup>ty jsi byla <sup>D</sup>prvá <br><sup>A</sup>Středověk neskončil <sup>G</sup>středověk <sup>D</sup>trvá<br><sup>A</sup>Jsme černí andělé a <sup>G</sup>ty jsi byla <sup>D</sup>prvá ...kluk",
    "youtube": "vaEERbPpIX0",
    "supermusic": "788912",
    "id": "3c64723b-4bb6-428b-a56c-41b5b1f811c6",
    "author": "Lucie",
    "title": "Černí andělé"
  },
  {
    "content": " <sup>C</sup> <sup>G7</sup> <sup>C</sup> Například východ slunce <sup>Emi7</sup> a vítr ve vět <sup>F</sup> vích<sup>Dmi, G7<br>C</sup>anebo píseň tichou <sup>Emi7</sup> jak padající <sup>F</sup> sníh<sup>Dmi, G<br>Dmi7</sup>tak to prý nelze <sup>G7</sup> koupit za <sup>C</sup> žádný pení <sup>F</sup> ze<br>jenže <sup>C</sup> zbejvá spousta <sup>F</sup> vě <sup>C</sup> cí <sup>D7</sup> a ty koupit <sup>G</sup> lze.<br><br>Ref: Jó vždyť víš štěstí je krásná věc<br>vždyť víš štěstí je krásná věc<br>stěstí je tá krásná a přepychová věc<br>ale prachy si <sup>G7</sup> za něj nekou <sup>C</sup> píš. <sup>G7</sup> <br><br>Jó kartón marlborek a taky porcelán<br>s modrejma cibulkama – tapety, parmazán<br>pět kilo uheráku nebo džínsy Calvin Klein<br>tak možná že to není štěstí ale je to fajn.<br><br>Například východ slunce a vítr ve větvích<br>anebo píseň tichou jak padající sníh<br>tak to prý nelze koupit za žádný peníze<br>jenže zbejvá spousta věcí - a ty koupit lze.<br><br>Takovej východ slunce je celkem v pořádku<br>peníze mám ale radši – ty stojej za hádku<br>a proto když se dočtu o zemětřesení<br>nebo o bouračce no tak řeknu: k neuvěření.<br><br>Ref:",
    "youtube": "nSaUetWOtTQ",
    "supermusic": "509",
    "id": "01c9799c-c98d-45e2-962d-e386a476e715",
    "author": "Richard Müller",
    "title": "Štestí je krásná věc"
  },
  {
    "content": "<sup>E</sup>Na,<sup>Cmi</sup>nanananaAnanananana <sup>E</sup><br><sup>E</sup>Najdem si <sup>C#mi</sup>místo <sup>G#mi</sup>kde se dobře <sup>F#mi</sup>kouří<br><sup>E</sup>Kde horké <sup>C#mi</sup>slunce <sup>G#mi</sup>do nápojů <sup>F#mi</sup>nepíchá<br><sup>H</sup>Kde vítr <sup>H/A</sup>snáší <sup>E</sup>šmolky ptačích <sup>A</sup>hovínek<br><sup>H</sup>Okolo <sup>A</sup>nás a <sup>H</sup>říká<br><br>Můžeme zkoušet co nám nejvíc zachutná<br>tak klidně se dívat jestli někdo nejde<br>Někdo kdo ví že už tady sedíme<br>A řekne: Nazdar, kluci<br> <br>® <sup>E</sup>Mám <sup>C#mi</sup>jednu ruku <sup>A</sup>dlou<sup>E</sup>hou<br>  <sup>E</sup>Mám <sup>C#mi</sup>jednu ruku <sup>A</sup>dlou<sup>E</sup>hou<br><br><sup>A</sup>Posaď se <sup>F#mi</sup>k nám <sup>C#mi</sup>necháme tě <sup>Hmi</sup>vymluvit<br><sup>A</sup>a vzpomeno<sup>F#mi</sup>ut si <sup>C#mi</sup>na ty naše <sup>Hmi</sup>úkoly<br><sup>E</sup>tu ruku nám <sup>Hmi</sup>dej a <sup>A</sup>odpočívej <sup>D</sup>v pokoji       <sup>E</sup><br><sup>A</sup>Tam na tom <sup>F#mi</sup>místě <sup>C#mi</sup>kde se dobře <sup>Hmi</sup>není.<br><br>® <sup>A</sup>Na, <sup>F#mi</sup>nananana - <sup>D</sup>ná <sup>A</sup>ná   4x<br>  <sup>Ami</sup>na, nananana  x<br>  <sup>D</sup>ná <sup>A</sup>ná ... <br>  <sup>A</sup>Mám <sup>F#mi</sup>jednu ruku <sup>D</sup>dlou<sup>A</sup>hou   x krát<br> ",
    "youtube": "2OsSbx-of70",
    "supermusic": "62470",
    "id": "ed54788e-48c4-4c80-a447-0574fa98fc3b",
    "author": "Buty",
    "title": "Mám jednu ruku dlouhou"
  },
  {
    "content": " <sup>A</sup> Ráno se <sup>Hmi</sup> otevřou mo <sup>D</sup> je oči voteklý<sup>A<br></sup>kopnu jen <sup>Hmi</sup> pod postel <sup>D</sup> pár špinavejch fu <sup>E</sup> seklí<br> <sup>A</sup> tělo mý je bo <sup>Hmi</sup> lavý a vedle <sup>D</sup> od sousedů hudba <sup>A</sup> zní<br> <sup>A</sup> nejde mi do <sup>Hmi</sup> hlavy kdo <sup>D</sup> je ta holka co tu spí<br>ta <sup>E</sup> holka co tu se mnou spí - je smu <sup>A</sup> tná<br><br>Jen divná nálada zůstane tu já jdu dál<br>a tak mi připadá jako bych jí něco vzal<br>Ještě se otočím a vrány v letu spočítám<br>vidím jí na očích , že letěj asi někam tak<br>to letěj asi někam tam<br><br>Ref: Tak to <sup>G</sup> vždycky začíná a každá <sup>D</sup> holka nevinná<br>právo <sup>C</sup> první noci měl jsem jenom <sup>G</sup> já<sup>D<br></sup>možná nez <sup>G</sup> bylo jí nic jen ty <sup>D</sup> slzy slaný víc<br>jak jí <sup>C</sup> padaj večer do klína mě <sup>D</sup> proklíná já vim<br><br>Je ráno musím jít a její oči vlhký sou<br>mý pravdě nevěří protože jí všichni lžou<br>lehám si pod obraz a někdy se tak cejtim sám<br>jsou na něm havrani co letěj asi někam tam<br>co letěj asi někam tam",
    "youtube": "FaynHWo6y9g",
    "supermusic": "1106",
    "id": "6c00e21b-c081-4078-b675-daa47895d81a",
    "author": "Kabát",
    "title": "Balada o špinavejch fuseklích"
  },
  {
    "content": " <sup>Dmi</sup> Výťah opäť nechodí tak <sup>C</sup> zdolať 13 poschodí<br> <sup>B</sup> zostáva mi <sup>C</sup> znova po svo <sup>Dmi</sup> jich<br>na schodoch čosi šramotí a neón kde tu nesvieti<br>ešte že sa po tme nebojím<br><br>Počuť hlasné stereo aj výstrahy pred neverou<br>ktosi čosi vŕta v paneloch<br>tatramatky ródeo sa mieša kde tu s operou<br>všetko počuť cestou po schodoch<br><br>Ref: Cestou <sup>Dmi</sup> po schodoch, <sup>Ami</sup> po schodoch<br> <sup>Dmi</sup> poznávam <sup>C</sup> poschodia<br>poznám <sup>Dmi</sup> po schodoch, <sup>Ami</sup> po zvukoch<br> <sup>B</sup> čo sme to <sup>A</sup> za ľu <sup>Dmi</sup> dia<br><br>Štekot smutnej kólie za premárnené prémie<br>vyhráža sa manžel rozvodom<br>Disko tenis árie kritika televízie<br>oddnes chodím iba po schodoch<br><br>Ref:",
    "youtube": "ZmPAcd5dLBI",
    "supermusic": "1049",
    "id": "4352f8ad-2861-4fc6-a460-c7f434fe5f42",
    "author": "Richard Müller",
    "title": "Po schodoch"
  },
  {
    "content": "│: <sup>Ami</sup> Na Kráľovej Holi <sup>E</sup> stojí strom zele <sup>Ami</sup> ný :│<br>│: <sup>Dmi</sup> vrch má naklonený <sup>C</sup> vrch má nak <sup>E</sup> lone <sup>Ami</sup> ný<br> <sup>Ami</sup> vrch má naklonený <sup>E</sup> k tej Slovenskej ze <sup>Ami</sup> mi :│<br><br>│: Odkážte, odpíšte tej mojej materi, :│<br>│: že mi svadba stojí, že mi svadba stojí,<br>že mi svadba stojí na Kráľovej holi :│<br><br>│: Odkážte, odpíšte, mojím kamarátom, :│<br>│: že už viac nepôjdem, že už viac nepôjdem,<br>že už viac nepôjdem na fraj za dievčaťom :│",
    "youtube": "YUyeAggHhro",
    "supermusic": "350238",
    "id": "0da01bbc-9c64-4721-b2b2-48de7b2d0cea",
    "author": "Lidovky",
    "title": "Na Kráľovej Holi"
  },
  {
    "content": "Jdu s <sup>C</sup> děravou patou, mám <sup>Am</sup> horečku zlatou,<br>jsem <sup>F</sup> chudý, jsem sláb a nemo <sup>C</sup> cen.<br> <sup>C</sup> Hlava mě pálí a <sup>Am</sup> v modravé dáli<br>se <sup>F</sup> leskne a <sup>G</sup> třpytí můj <sup>C</sup> sen.<br><br>Kraj pod sněhem mlčí, tam stopy jsou vlčí,<br>tam zbytečně budeš mi psát.<br>Sám v dřevěné boudě sen o zlaté hroudě,<br>já nechám si tisíc krát zdát.<br><br>Ref: <sup>C</sup> Severní <sup>C7</sup> vítr je <sup>F</sup> krutý,<br>počítej <sup>C</sup> lásko má s <sup>G7</sup> tím.<br> <sup>C</sup> K nohám ti <sup>C7</sup> dám zlaté <sup>F</sup> pruty,<br>nebo se <sup>C</sup> vůbec <sup>G</sup> nevrátím. <sup>C</sup> <br><br>Tak zarůstám vousem a vlci už jdou sem,<br>už slyším je výt blíž a blíž.<br>Už mají mou stopu, už větří, že kopu<br>svůj hrob a že stloukám si kříž.<br><br>Zde leží ten blázen, chtěl dům a chtěl bazén<br>a opustil tvou krásnou tvář.<br>Má plechovej hrnek a pár zlatejch zrnek,<br>a nad hrobem polární zář.<br><br>Ref:",
    "youtube": "QQIbhuRgg_8",
    "supermusic": "99896",
    "id": "57df2aa4-1d29-47c3-b2f1-89320d9fc49f",
    "author": "Jaroslav Uhlíř",
    "title": "Severní vítr"
  },
  {
    "content": " <sup>F</sup> <sup>C</sup> <sup>G</sup> <sup>C</sup> <br> <sup>F</sup> To je ta <sup>C</sup> písnič <sup>G</sup> ka pro te <sup>C</sup> be<br> <sup>F</sup> Z <sup>C</sup> autobu <sup>G</sup> sá <sup>C</sup> ku<br>mrazi <sup>F</sup> vé <sup>C</sup> a modravé <sup>G</sup> obrysy <sup>C</sup> mraků<br> <sup>F</sup> ptá <sup>C</sup> ků a pane <sup>G</sup> lá <sup>C</sup> ků<br><br>Nic není co by stálo aspoň za něco<br>A něco nestojí ani za to<br>Nic není co by stálo aspoň za něco<br>A něco nestojí ani za to<br><br>Co za to stojí to neví nikdo<br>Někdo možná snad Ale nikdo neví kdo<br>Co za to stojí to neví nikdo Nikdo<br>To je tá písnička pro tebe<br><br> <sup>F</sup> To je ta <sup>C</sup> písnič <sup>G</sup> ka pro te <sup>C</sup> be<br> <sup>F</sup> Z <sup>C</sup> autobu <sup>G</sup> sá <sup>C</sup> ku<br>mrazi <sup>F</sup> vé <sup>C</sup> a modravé <sup>G</sup> obrysy <sup>C</sup> mraků<br> <sup>F</sup> ptá <sup>C</sup> ků a pane <sup>G</sup> lá <sup>C</sup> ků<br>pro tebe ..<br>pro tebe ..<br>pro tebe ..",
    "youtube": "CWXJG8zBQAc",
    "supermusic": "2365",
    "id": "e74c0978-8ad2-4aee-b370-f9fdb05ddce7",
    "author": "Mňága A Žďorp",
    "title": "Písnička pro tebe"
  },
  {
    "content": " <sup>G</sup> Léta tam <sup>D</sup> stál, <sup>C</sup> stojí tam <sup>G</sup> dál,<br> <sup>G</sup> pivovar <sup>D</sup> u cesty, <sup>C</sup> každý ho znal <sup>D</sup> .<br> <sup>G</sup> Léta tam <sup>D</sup> stál, <sup>C</sup> stát bude dál <sup>G</sup> ,<br> <sup>G</sup> ten kdo zná <sup>D</sup> Jarošov, <sup>C</sup> zná pi <sup>D</sup> vovar <sup>G</sup> .<br><br>Ref: <sup>C</sup> Bílá <sup>D</sup> pěna, lá <sup>G</sup> hev oro <sup>Emi</sup> sená,<br> <sup>C</sup> chmelový <sup>D</sup> nektar já <sup>G</sup> znám.<br> <sup>C</sup> Jen jsem to <sup>D</sup> zkusil a <sup>G</sup> jednou se <sup>Emi</sup> napil,<br> <sup>C</sup> od těch dob <sup>D</sup> žízeň <sup>G</sup> mám.<br><br>Bída a hlad, kolem šel strach,<br>když bylo piva dost mohlo se smát.<br>Třista let stál, stát bude dál,<br>ten kdo zná Jarošov, zná pivovar.",
    "youtube": "aMzHF58i-kk",
    "supermusic": "1323",
    "id": "cecc1735-c054-464b-ad5b-f30a71387708",
    "author": "Argema",
    "title": "Jarošovský pivovar"
  },
  {
    "content": "<sup>C</sup>Ať bylo mne i <sup>F</sup>jí<br>tak <sup>G7</sup>16 <sup>C</sup>let,<br><sup>C</sup>zeleným údo<sup>Ami</sup>lím <br>sem si jí <sup>Dmi</sup>ved.<br><sup>C</sup>Byla krásna, to <sup>C7</sup>vím<br>a já mnel strach<br><sup>F</sup>jak říct, <sup>Fmi</sup>když na řa <sup>C</sup>sách<br>slzu <sup>G7</sup>mám velkú jako <sup>C</sup>hrách.<br>REF: <sup>C7</sup>Sbohem <sup>F</sup>lásko nech mne <sup>Dmi</sup>jít<br>nech mne <sup>Emi</sup>jít bude <sup>Ami</sup>klid,<br>žádnej <sup>Dmi</sup>pláč už nespra <sup>G7</sup>ví,<br>ty mí <sup>C</sup>nohy toula <sup>C7</sup>ví.<br>Já tě <sup>F</sup>vážne mnel moc <sup>Dmi</sup>rád,<br>co ti <sup>Emi</sup>víc mužu <sup>Ami</sup>dát,<br>nejsem <sup>Dmi</sup>žádnej ide <sup>G7</sup>ál,<br>nech mne <sup>C</sup>jít zas <sup>F</sup>o dum <sup>C</sup>dál.<br>A tak šel čas a ja se toulám dál,<br>v kolika údolí sem takhle stál<br>hledal slúvka co sou jak hojivej fáč<br>Búh ví co sem to zač, že prinášim všem jenom pláč.<br>REF: Sbohem lásko nech mne jít<br>nech mne jít bude klid,<br>žádnej pláč už nespraví,<br>ty mí nohy toulaví.<br>Já tě vážne mnel moc rád,<br>co ti víc mužu dát,<br>nejsem žádnej ideál,<br>nech mne jít zas o dum dál.<br>Já nevím kde se to v člověku bere,<br>ten neklid co ho tahá z místa na místo,<br>co ho nenechá, aby byl sám ze sebou spokojenej,<br>jako väčšina ostatných, aby se usadil<br>aby delal to co má a říkal co se od nej čeká,<br>já proste nemúžu zústat na jedným míste, nemúžu opravdu, fakt.",
    "youtube": "tlDbVfPDCQU",
    "supermusic": "183980",
    "id": "0f33a967-26d2-4539-bcc7-6284c91f6b09",
    "author": "Waldemar Matuška",
    "title": "Sbohem lásko"
  },
  {
    "content": " <sup>D</sup> Hodíme si <sup>A</sup> mincí trnovou <sup>G</sup> <sup>A</sup> <br> <sup>D</sup> Oči mých <sup>A</sup> smutků nenech <sup>G</sup> plát - <sup>A</sup> jsem tady<br> <sup>D</sup> Tvůj vodní <sup>A</sup> hrad je pod vo <sup>G</sup> dou – jó <sup>A</sup> , jó<br> <sup>D</sup> Votoč se - já <sup>A</sup> vo to budu stát<sup>G, A</sup>umírám<br><br>Ref: <sup>D</sup> Miláčku <sup>A</sup> pleteš si pojmy a z <sup>G</sup> lásky <sup>D</sup> zbyly jen dojmy<br> <sup>D</sup> Vzpomínat i  <sup>A</sup> zapomínat <sup>G</sup> a voči při tom <sup>D</sup> zavírat<br><br>Není to láska není to zlo, jenom se nám to nepovedlo<br>Miláčku kde je to kouzlo, to který chtělo při mně stát<br><br>Skřípni mi prsty do tvých dveří<br>Věřím že věříš - nikdo nám nevěří, né..<br>Schovej hlavu pod polštář, tak dobře to tam znáš<br>Tak dobře jako já - usínám<br><br>Ref:",
    "youtube": "hA5HWeg_Il0",
    "supermusic": "845",
    "id": "dbbdadd7-1b69-44fd-aa1a-0172fba0f943",
    "author": "Wanastowi Vjecy",
    "title": "Kouzlo"
  },
  {
    "content": " <sup>A</sup> Není nutno, není nutno, aby bylo přímo vese <sup>Hmi</sup> lo,<br> <sup>E7</sup> hlavně nesmí býti smutno, natož aby se breče <sup>A</sup> lo. <sup>E</sup> <br><br>Chceš-li, trap se, že ti v kapse zlaté mince nechřestí,<br>nemít žádné kamarády, tomu já říkám neštěstí.<br><br>Ref: Nemít <sup>F#mi</sup> prachy – <sup>A</sup> nevadí,<br>nemít <sup>F#mi</sup> srdce – <sup>A</sup> vadí,<br>zažít <sup>F#mi</sup> krachy – <sup>A</sup> nevadí,<br>zažít <sup>F#mi</sup> nudu – <sup>D</sup> jó, to <sup>E</sup> vadí.<br>",
    "youtube": "O4EfF94uMJI",
    "supermusic": "543",
    "id": "d9a9e6b0-4eb9-4536-9d85-965a49e2e94f",
    "author": "Jaroslav Uhlíř",
    "title": "Není nutno"
  },
  {
    "content": " <sup>A</sup> Na těch panských <sup>D</sup> lukách<br> <sup>E</sup> ztratil jsem já <sup>A</sup> dukát<br>|: <sup>A</sup> Kdo mi ho promění <sup>D</sup> kdo mi ho promění<br> <sup>E</sup> Milá doma <sup>A</sup> není :|<br><br>Pojdem do hospody kde cigání hrají<br>|: Ti mi ho promění, ti mi ho promění<br>Ti peníze mají :|<br><br>Když ho nepromění, dám ho do cimbála<br>|: Muzika bude hrát muzika bude hrát<br>do bílého rána :|<br><br>Do bílého rána muzika nám hrála<br>|: Všechny panny tancovaly všechny panny tancovaly<br>jen ta moja stála :|",
    "youtube": "25we5Dr6hEw",
    "supermusic": "5653",
    "id": "e8dcc974-d35c-47df-8785-86ee8bbff1bd",
    "author": "Lidovky",
    "title": "Na těch panských lukách"
  }
]