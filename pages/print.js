// @flow

import * as React from "react"
import { connect } from "react-redux"
import { getSongs } from "../app/main/selectors"
import { pageLoaded } from "../app/main/actions"


type Props = {
    list: Array<{ id: string, author: string, title: string, content: ?string }>,
    onLoad: () => void,
}

class PrintPage extends React.Component<Props> {
    props: Props

    componentDidMount() {
        this.props.onLoad()
    }

    render() {
        return (
            <div className="container">
                <div style={{
                    padding: 12,
                }}>
                    {this.props.list.map(song => (
                        <div>
                            {song.title && (
                                <h1 style={{ fontSize: 28 }}>
                                    {song.author} – {song.title}
                                </h1>
                            )}
                            {song.content && (
                                <div
                                    className="song-content-wrapper-print"
                                    dangerouslySetInnerHTML={{ __html: `<p>${song.content}</p>` }} />
                            )}
                            <div style={{ "page-break-before": "always" }} />
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    list: getSongs(state),
})

const mapDispatchToProps = {
    onLoad: pageLoaded,
}

export default connect(mapStateToProps, mapDispatchToProps)(PrintPage)
