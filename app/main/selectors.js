// @flow

import type { StateObject } from "../state"
import type { MainState } from "./reducer"
import type { Song } from "../types"
import { normalizeSearchText } from "./utils"


export const getMainStateObject = (state: StateObject): MainState =>
    state.main

export const getSelectedSongId = (state: StateObject): ?string =>
    getMainStateObject(state).songId

export const getSongs = (state: StateObject): Song[] =>
    getMainStateObject(state).songs.sort((a, b) => getTitleToCompare(a).localeCompare(getTitleToCompare(b)))

export const getSearchText = (state: StateObject): ?string =>
    getMainStateObject(state).searchText

export const getSelectedSong = (state: StateObject): ?Song => {
    const songId = getSelectedSongId(state)
    if (!songId) {
        return null
    }
    return getSongs(state).find(item => item.id === songId)
}

export const getTitleToCompare = (song: Song) => song.author + " - " + song.title

export const getSelectedSongTitle = (state: StateObject): ?string => {
    const song = getSelectedSong(state)
    return song ? song.title : null
}

export const getSelectedSongContent = (state: StateObject): ?string => {
    const song = getSelectedSong(state)
    if (!song || !song.content) {
        return null
    }

    return song.content
        .replace(/<a(.*?)">/g, "")
        .replace(/<\/a>/g, "")
        .replace(/<br>/g, "<br />")
}

export const getSelectedSongVideoURL = (state: StateObject): ?string => {
    const song = getSelectedSong(state)
    if (!song || !song.youtube) {
        return null
    }
    return `https://www.youtube.com/embed/${song.youtube}`
}

export const getSongList = (state: StateObject): Array<{ id: string, title: string }> => {
    const search = getSearchText(state)
    return getSongs(state)
        .filter(item => !search || item.normalized.includes(normalizeSearchText(search)))
}
