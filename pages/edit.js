// @flow

import * as React from "react"
import { connect } from "react-redux"
import { getSelectedSong } from "../app/main/selectors"
import { pageLoaded, saveSongClicked } from "../app/main/actions"
import type { Song } from "../app/types"


type Props = {
    song: ?Song,
    onLoad: () => void,
    onSaveClicked: (id: string, content: string, title: string, author: string) => void,
}
type State = {
    newContent: ?string,
    newAuthor: ?string,
    newTitle: ?string,
}

class EditSong extends React.Component<Props, State> {
    props: Props

    constructor(props: Props) {
        super(props)
        this.state = {
            newContent: props.song ? props.song.content : null,
            newAuthor: props.song ? props.song.author : null,
            newTitle: props.song ? props.song.title : null,
        }
    }

    componentDidMount() {
        this.props.onLoad()
    }

    onTextAreaChange = (e: any) => {
        this.setState({ newContent: e.target.value })
    }

    onAuthorChange = (e: any) => {
        this.setState({ newAuthor: e.target.value })
    }

    onTitleChange = (e: any) => {
        this.setState({ newTitle: e.target.value })
    }

    onSave = (e: any) => {
        e.preventDefault()
        this.props.onSaveClicked(this.props.song.id, this.state.newContent, this.state.newTitle, this.state.newAuthor)
    }

    render() {
        return (
            <div className="container"  style={{ padding: "1rem" }}>
                {this.props.song && (
                    <div>
                        <div>
                            <h1>{this.props.song.title}</h1>
                            <input
                                size={70}
                                style={{ padding: "0.5rem 1rem", lineHeight: 2}}
                                value={this.state.newAuthor}
                                onChange={this.onAuthorChange} />
                            <br />
                            <br />
                            <input
                                size={70}
                                style={{ padding: "0.5rem 1rem", lineHeight: 2}}
                                value={this.state.newTitle}
                                onChange={this.onTitleChange} />
                            <br />
                            <br />
                            <textarea
                                rows={15}
                                cols={150}
                                style={{ padding: "1rem", lineHeight: 2}}
                                onChange={this.onTextAreaChange}>
                                {this.props.song.content.replace(/<br>/g, "\n")}
                            </textarea>
                            <br />
                            <br />
                        </div>
                        <div>
                            <input type="button" value={"SAVE"} onClick={this.onSave} />
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    song: getSelectedSong(state),
})

const mapDispatchToProps = {
    onLoad: pageLoaded,
    onSaveClicked: saveSongClicked,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditSong)
