// @flow

import Router from "next/router"
import withRedux from "next-redux-wrapper"
import { initStore } from "../app/store"
import type { NextContext } from "../utils/types"
import { getCookie } from "../lib/cookies"
// $FlowFixMe
import "../styles/app.less"


const app = (
    Page: any,
    options?: {|
        requireAuth?: boolean,
    |},
) => {
    const { requireAuth } = options || {}

    const App = () => withRedux(initStore, null, null)(Page)

    App.getInitialProps = async (context: NextContext) => {
        const cookie = getCookie("twbrds-session", context.req)
        const isAuthenticated = !!cookie

        if (requireAuth && !isAuthenticated) {
            redirectToSignIn(context)
        }

        return {}
    }

    return App
}

const redirectToSignIn = (context: NextContext) => {
    const { asPath, res } = context
    const redirectUrlKey = "redirectUrl"
    const redirectUrl = encodeURIComponent(asPath)
    const href = {
        pathname: "/login",
        query: { [redirectUrlKey]: redirectUrl },
    }
    if (res) {
        res.writeHead(303, {
            Location: `${href.pathname}?${redirectUrlKey}=${redirectUrl}`,
        })
        res.end()
    }
    else {
        Router.replace(href.pathname)
    }
}

export default app
