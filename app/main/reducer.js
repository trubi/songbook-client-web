// @flow

import { SEARCH_CHANGED, SONG__SELECTED, SONG__UPDATED, SONGS_LOADED } from "./actions"
import type { Song } from "../types"


export type MainState = {|
    songs: Song[],
    songId: ?string,
    searchText: ?string,
|}

export const createDefaultMainState = () => ({
    songs: [],
    songId: null,
    searchText: null,
})

export const mainReducer = (state: ?MainState, action: Object): MainState => {
    if (!state) {
        return createDefaultMainState()
    }

    switch (action.type) {
        case SONG__SELECTED:
            return {
                ...state,
                songId: action.payload.songId,
                searchText: null,
            }

        case SONGS_LOADED:
            return {
                ...state,
                songs: action.payload.songs,
            }

        case SONG__UPDATED:
            return {
                ...state,
                songs: state.songs.map(s => (s.id === action.payload.song.id ? action.payload.song : s)),
            }

        case SEARCH_CHANGED:
            return {
                ...state,
                searchText: action.payload.text,
                songId: null,
            }
    }

    return state
}
