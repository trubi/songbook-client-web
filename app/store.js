// @flow

import { createStore, applyMiddleware } from "redux"
import { createEpicMiddleware } from "redux-observable"
import { composeWithDevTools } from "redux-devtools-extension"
import { reducer, initialState } from "./reducer"
import configureEpics from "../lib/configureEpics"
import type { StateObject } from "./state"


const deps = {
}

export const initStore = (state: StateObject = initialState) => {
    return createStore(
        reducer,
        state,
        composeWithDevTools(applyMiddleware(
            createEpicMiddleware(configureEpics(deps)),
        )),
    )
}
