// @flow

import { combineReducers } from "redux"
import type { StateObject } from "./state"
import { createDefaultMainState, mainReducer } from "./main/reducer"


export const initialState: StateObject = {
    main: createDefaultMainState(),
}

export const reducer = combineReducers({
    main: mainReducer,
})
