import { normalizeSearchText } from "../../searchbar/search"


describe("normalizeSearchText", () => {
    it("normalizeSearchText", () => {
        return expect(normalizeSearchText("Ahoj Péťo, bude nám tu príma")).toEqual("ahoj peto, bude nam tu prima")
    })
})
