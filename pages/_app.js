// @flow

import React from "react"
import { Provider } from "react-redux"
import App, { Container } from "next/app"
import withRedux from "next-redux-wrapper"
import { initStore } from "../app/store"
import { songSelected } from "../app/main/actions"


class MyApp extends App {

    static async getInitialProps({ Component, ctx }) {
        if (ctx && ctx.query && ctx.query.s) {
            ctx.store.dispatch(songSelected(ctx.query.s))
        }
        else {
            ctx.store.dispatch(songSelected(null))
        }

        return {
            pageProps: {
                ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
            },
        }
    }

    render() {
        const { Component, pageProps, store } = this.props
        return (
            <Container>
                <Provider store={store}>
                    <Component {...pageProps} />
                </Provider>
            </Container>
        )
    }
}

export default withRedux(initStore, { debug: false })(MyApp)
