
module.exports = {
    testPathIgnorePatterns: [
        "/node_modules/",
        "/.next/",
        "/.idea/",
        "/.flow-typed/",
        "/.static/",
    ],
}
