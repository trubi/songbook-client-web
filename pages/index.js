// @flow

import * as React from "react"
import Main from "../app/main/Main"


const IndexPage = () => {
    return <Main />
}

export default IndexPage
