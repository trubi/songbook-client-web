// @flow

import { format as dateFnsFormat } from "date-fns"


export const formatPrice = (amount: number) => {
    const amountInDollars = amount / 100
    return amountInDollars.toLocaleString("en-US", {
        style: "currency",
        currency: "USD",
        maximumFractionDigits: 0,
        minimumFractionDigits: 0,
    })
}

export const formatDateTime = (date: string | Date, format: ?string = null) => {
    if (format) {
        return dateFnsFormat(date, format)
    }
    else {
        return new Date(date).toLocaleString()
    }
}
