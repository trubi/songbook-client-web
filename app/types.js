// @flow


export type Song = {
    id: string,
    author: string,
    title: string,
    normalized: string,
    content: ?string,
    youtube: ?string,
}
