// @flow

import * as Rx from "rxjs"
import Router from "next/router"
import { PAGE_LOADED, SAVE_SONG_CLICKED, SCRAPE_SONG_CLICKED, songsLoaded, songUpdated, TRANS_CLICKED } from "./actions"
import { createHttpRequest } from "../../lib/http"
import { normalizeSearchText } from "./utils"
import { getSelectedSong } from "./selectors"


export const pageLoadedEpic = (actions$: Object) =>
    actions$.ofType(PAGE_LOADED)
        .switchMap(() => Rx.Observable
            .defer(() => createHttpRequest({
                method: "GET",
                // url: "http://songbook.us-east-1.elasticbeanstalk.com/song",
                url: "http://localhost:3011/song",
            }))
            .map((result) => {
                return songsLoaded(result.map(item => ({
                    id: item.id,
                    author: item.author,
                    title: item.title,
                    normalized: normalizeSearchText(item.author + " - " + item.title),
                    content: item.content,
                    youtube: item.youtube,
                })))
            }))

const cleanUpContent = (content: string): string => {
    return content
        .replace(/\n/g, "<br>")
        .replace(/ *<br> */g, "<br>")
        .replace(/ ?<sup>([a-zA-Z0-9#]{1,7})<\/sup> ?/g, match => " " + match + " ")
        .replace(/ {2,}/g, " ")
}

export const saveSongEpic = (actions$: Object, deps) =>
    actions$.ofType(SAVE_SONG_CLICKED)
        .switchMap((action) => {
            const song = getSelectedSong(deps.getState())
            const data = {
                id: action.payload.id,
                author: action.payload.author,
                title: action.payload.title,
                content: cleanUpContent(action.payload.content),
            }
            return Rx.Observable
            .defer(() => createHttpRequest({
                method: "POST",
                url: `http://localhost:3011/song`,
                headers: {
                    "content-type": "application/json"
                },
                body: data,
            }))
            .switchMap((result) => {
                console.log(result)
                Router.push("/")
                return [
                    songUpdated({
                        ...song,
                        author: data.author,
                        title: data.title,
                        content: data.content,
                    })
                ]
            })
        })

export const scrapeSongEpic = (actions$: Object) =>
    actions$.ofType(SCRAPE_SONG_CLICKED)
        .switchMap((action) => Rx.Observable
            .defer(() => createHttpRequest({
                method: "POST",
                url: `http://localhost:3011/song/scrape`,
                headers: {
                    "content-type": "application/json"
                },
                body: {
                    songId: action.payload.songId,
                },
            }))
            .switchMap((result) => {
                console.log(result)
                Router.push("/")
                return []
            }))

const chords = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "H"]

const getRootChord = (chord: string): string => {
    return chord.match(/^([a-zA-Z]#?)/)[0]
}

const getNewChordIndex = (root: string, amount: number): number => {
    const index = chords.indexOf(root) + amount
    if (index < 0) {
        return chords.length + index
    }
    if (index >= chords.length) {
        return index - chords.length
    }
    return index
}

const transposeChord = (chord: string, amount: number): string => {
    const root = getRootChord(chord)
    const index = getNewChordIndex(root, amount)
    return chords[index] + chord.substr(root.length)
}

const transposeContent = (content: string, amount: number): string => {
    return content.replace(
        /<sup>([a-zA-Z0-9#]{1,7})<\/sup>/g,
        match => "<sup>" + transposeChord(match.substring(5, match.length - 6), amount) + "</sup>",
    )
}

export const transSongClickedEpic = (actions$: Object, deps) =>
    actions$.ofType(TRANS_CLICKED)
        .switchMap(action => {
            console.log(action)
            const song = getSelectedSong(deps.getState())
            console.log(song.content)
            const content = cleanUpContent(transposeContent(song.content, action.payload.amount))
            console.log(content)

            return Rx.Observable
                .defer(() => createHttpRequest({
                    method: "POST",
                    url: `http://localhost:3011/song`,
                    headers: {
                        "content-type": "application/json"
                    },
                    body: {
                        id: song.id,
                        author: song.author,
                        title: song.title,
                        content: cleanUpContent(content),
                    },
                }))
                .switchMap((result) => {
                    console.log(result)
                    return [
                        songUpdated({
                            ...song,
                            content: content,
                        })
                    ]
                })
                .catch(err => {
                    console.log(err)
                    return []
                })
        })

export default [
    pageLoadedEpic,
    saveSongEpic,
    scrapeSongEpic,
    transSongClickedEpic,
]
