// @flow

import { combineEpics } from "redux-observable"
import epics from "../app/epics"
import type { StateObject } from "../app/state"


export type EpicDependencies = {
    getState: () => StateObject,
    dispatch: ({ type: string }) => void,
}

const configureEpics = (deps: Object) =>
    (action$: Object, miniStore: Object) => combineEpics(...epics)(action$, { ...deps, ...miniStore })

export default configureEpics
