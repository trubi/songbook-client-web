// @flow


export type NextUrlParam = {
    asPath: string,
    pathname: string,
    query: { [string]: string },
}

export type NextContext = {
    pathname: string,
    query: any,
    asPath: string,
    req?: any,
    res?: any,
    jsonPageRes?: any,
    err?: any,
}
