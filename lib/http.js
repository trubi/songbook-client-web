// @flow

import fetch from "isomorphic-fetch"


type HttpRequestType = {
    method: string,
    url: string,
    headers: Object,
    body?: Object,
}

export const createHttpRequest = (data: HttpRequestType): Promise<any> => {
    const options = {
        method: data.method,
        headers: data.headers,
        body: data.body === null ? undefined : JSON.stringify(data.body),
        timeout: 30 * 1000,
    }

    return fetch(data.url.replace(/\s/g, ""), options)
        .then(response => (response.status >= 200 && response.status < 300
            ? getResponseBody(response)
            : getResponseBody(response)
                .then(body => Promise.reject(new Error({
                    status: response.status,
                    body: body,
                })))))
        .catch(error => {
            if (error.status) {
                console.log("error", error)
            }
            else {
                console.log("error without status", error)
            }
            return Promise.reject(new Error(error))
        })
}

const getResponseBody = (response) => {
    if (response.headers.get("content-type").includes("json")) {
        return response.json().catch(err => Promise.resolve({}))
    }
    else if (response.headers.get("content-type").includes("text")) {
        return response.text().catch(err => Promise.resolve({}))
    }
    else {
        return Promise.resolve({})
    }
}
