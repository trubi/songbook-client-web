// @flow

import * as React from "react"
import { Layout } from "antd"


type Props = {
    children: React.Node,
}

const LoginLayout = (props: Props) => {
    return (
        <div>
            <Layout>
                {props.children}
            </Layout>
        </div>
    )
}

export default LoginLayout
