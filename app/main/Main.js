// @flow

import * as React from "react"
import { connect } from "react-redux"
import { Button, Icon, Input } from "antd"
import Link from "next/link"
import { getSelectedSong, getSelectedSongVideoURL, getSongList, getTitleToCompare } from "./selectors"
import { pageLoaded, scrapeSongClicked, searchChanged, songSelected, transClicked } from "./actions"
import type { Song } from "../types"


type Props = {
    list: Array<{ id: string, title: string }>,
    song: ?Song,
    videoUrl: ?string,
    onLoad: () => void,
    handleClick: (id: string) => void,
    searchChanged: (text: ?string) => void,
    onInsertClicked: (songId: string) => void,
    onTransClicked: (songId: string, amount: number) => void,
}

type State = {
    insertField: string,
}

class Main extends React.Component<Props, State> {
    props: Props

    constructor(props: Props) {
        super(props)
        this.state = {
            insertField: "",
        }
    }

    componentDidMount() {
        this.props.onLoad()
    }

    renderContent = () => {
        if (this.props.song) {
            return (
                <div style={{ padding: 15 }}>
                    {this.props.song.title && (
                        <h1 style={{ fontSize: 34 }}>
                            {this.props.song.author} – {this.props.song.title}
                        </h1>
                    )}
                        <div style={{ float: "right" }}>
                            <br /><br />
                            {this.props.videoUrl && (
                                <iframe
                                    title="Video"
                                    width="210"
                                    height="157"
                                    src={this.props.videoUrl} />
                            )}

                            <Link href={{ pathname: "/edit", query: { s: this.props.song.id } }}>
                                <a>EDIT</a>
                            </Link>

                            <Button onClick={() => this.props.onTransClicked(this.props.song.id, -1)}>-1</Button>
                            <Button onClick={() => this.props.onTransClicked(this.props.song.id, 1)}>+1</Button>
                        </div>
                    {this.props.song.content && (
                        <div
                            className="song-content-wrapper"
                            dangerouslySetInnerHTML={{ __html: `<p>${this.props.song.content}</p>` }} />
                    )}
                </div>
            )
        }
        else {
            return (
                <div>
                    <Link href={{ pathname: "/print" }}>
                        <a>PRINT</a>
                    </Link>
                    <br /><br /><br />
                    {this.props.list.map(item => (
                        <div key={item.id} style={{ paddingBottom: 6 }}>
                            <Link href={{ pathname: "/", query: { s: item.id } }}>
                                <a>{getTitleToCompare(item)}</a>
                            </Link>
                        </div>
                    ))}
                </div>
            )
        }
    }

    handleSearchChanged = (e) => {
        this.props.searchChanged(e.target.value)
    }

    handleInsertFieldChanged = (e) => {
        this.setState({
            insertField: e.target.value,
        })
    }

    onInsertBtnClicked = () => {
        this.props.onInsertClicked(this.state.insertField)
    }

    render() {
        return (
            <div className="container">
                <div style={{
                    padding: 8,
                    backgroundColor: "silver",
                }}>
                    <Input
                        onChange={this.handleSearchChanged}
                        suffix={<Icon type="search" className="certain-category-icon" />} />
                </div>

                <div style={{
                    padding: 8,
                    backgroundColor: "silver",
                }}>
                    <input size={50} onChange={this.handleInsertFieldChanged} />
                    <input type="button" value="INSERT" onClick={this.onInsertBtnClicked} />
                </div>

                <div style={{
                    padding: 12,
                }}>
                    {this.renderContent()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    list: getSongList(state),
    song: getSelectedSong(state),
    videoUrl: getSelectedSongVideoURL(state),
})

const mapDispatchToProps = {
    onLoad: pageLoaded,
    handleClick: songSelected,
    searchChanged: searchChanged,
    onInsertClicked: scrapeSongClicked,
    onTransClicked: transClicked,
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
