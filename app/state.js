// @flow

import type { MainState } from "./main/reducer"


export type StateObject = {
    main: MainState,
}
